The following is software developed by Jeffrey Cochran in cooperation with professors from Stevens Institute of Technology.
Art assets have been contributed by Coray Spencer, and some components may be extended by Hao Kang. 

This project has grown out of a group project under the guidance of George Kamberov and Phillipos Morodhai, however 
at this time all source contained herein has been developed solely by Jeff Cochran.

Notwithstanding, the SLAM sytem, PTAMM, was originally developed by Georg Klein from Oxford University 
(http://www.robots.ox.ac.uk/~gk/). The PTAMM system used in AR Pacman has been modified to suit the needs of 
the project.

I, Jeffrey Cochran, retain all rights and ownership of this software to the maximal extend allowable by law and
the licenses of this software's constituent systems. Note that this will probably change once I figure out how I 
should license this software. When I decide how to license it, this readme will be modified.