package edu.stevens.arpac;

import android.util.Log;
import android.view.MotionEvent;
import edu.stevens.arpac.graphics.GameCamera;

/**
 * This class handles UI events that move the camera in the event that we're not being
 * controlled by the PTAMM location provider
 * @author Jeff Cochran
 *
 */
public class PlayerCamera 
{
	private static final String TAG = "PlayerCamera";
	private enum Operation { none, rot_left, rot_right, move_in, move_out, rot_down, rot_up };
	private Operation[] touches = new Operation[10];
	private boolean[] performed = new boolean[4];
	
	private GameCamera cam;
	private float scrnWidth;
	private float scrnHeight;
	public PlayerCamera(GameCamera cam, float width, float height)
	{
		this.cam = cam;
		this.scrnWidth = width;
		this.scrnHeight = height;
		
		for(int i = 0; i < 10; i++)
		{
			touches[i] = Operation.none;
		}
	}
	
	public void moveCamera(float deltaTime)
	{
		for(int i = 0; i < 4; i++)
		{
			performed[i] = false;
		}
		deltaTime *= 0.001f; 
		for(int i = 0; i < 10; i++)
		{
			switch(touches[i])
			{
			case none:
				break;
			case rot_left:
				if(!performed[0])
				{
					cam.rotate(24.0f * deltaTime, 0.0f, 1.0f, 0.0f);
				}
				performed[0] = true;
				break;
			case rot_right:
				if(!performed[1])
				{
					cam.rotate(-24.0f * deltaTime, 0.0f, 1.0f, 0.0f);
				}
				performed[1] = true;
				break;
			case move_in:
				if(!performed[2])
				{
					cam.move(0.0f, 0.0f, -20.0f * deltaTime);
				}
				performed[2] = true;
				break;
			case move_out:
				if(!performed[3])
				{
					cam.move(0.0f, 0.0f, 20.0f * deltaTime);
				}
				performed[3] = true;
				break;
			default:
				Log.d(TAG, "moveCamera encountered invalid state in touches[] array");
				break;
			}
		}
	}
	
	public boolean handleTouch(int action, int pointerId, float x, float y)
	{
		if((action == MotionEvent.ACTION_DOWN) || 
				   (action == MotionEvent.ACTION_POINTER_DOWN))
				{
					if(Math.min(x, scrnWidth - x) < Math.min(y, scrnHeight - y))
					{
						if(x < (scrnWidth - x))
						{
							touches[pointerId] = Operation.rot_left;
						} else {
							touches[pointerId] = Operation.rot_right;
						}	
					}
					else
					{
						if(y < (scrnHeight - y))
						{
							touches[pointerId] = Operation.move_in;
						} else {
							touches[pointerId] = Operation.move_out;
						}
					}
				}
				else if((action == MotionEvent.ACTION_UP) || 
						(action == MotionEvent.ACTION_CANCEL) || 
						(action == MotionEvent.ACTION_POINTER_UP)) 
				{
					touches[pointerId] = Operation.none;
				}
				return true;
	}
}
