package edu.stevens.arpac;

import java.util.Arrays;

import edu.stevens.arpac.engine.Actor;
import edu.stevens.arpac.engine.ITickable;
import edu.stevens.arpac.math.Maths;
import edu.stevens.arpac.math.Quaternion;

/**
 * A controller for a ghost enemy
 * @author Jeff Cochran
 *
 */
public class GhostController implements ITickable
{
	@SuppressWarnings("unused")
	private static final String TAG = "GhostController";
	private static final float RAD_TO_DEG = (float) (180.0f / Math.PI);
	private static float SPEED_SECS = 5.0f;
	private Actor actor;
	
	/**
	 * Instantiate a new GhostController with the given actor and x y position
	 * @param act
	 * @param x
	 * @param y
	 */
	public GhostController(Actor act, float x, float y)
	{
		actor = act;
		act.setPosition(x, y);
	}
	
	
	private float[] target = new float[4];
	private float[] tmpVec = new float[4];
	/**
	 * Set target for the ghost to move towards
	 * @param x
	 * @param y
	 */
	public void setTarget(float x, float y)
	{
		float[] actPos;
		
		target[0] = x - actor.pos[0];
		target[1] = 0.0f;
		target[2] = y - actor.pos[1];
		target[3] = 1.0f;
		actPos = Arrays.copyOf(target, 4);
		
		tmpVec[0] = 0.0f;
		tmpVec[1] = 0.0f;
		tmpVec[2] = 1.0f;
		tmpVec[3] = 0.0f;
		
		Quaternion orient = actor.getOrientation();
		orient.rotate(tmpVec, tmpVec);
		float theta = Maths.angleBetween(target, tmpVec);
		theta *= RAD_TO_DEG;
		tmpVec[0] += actor.pos[0];
		tmpVec[2] += actor.pos[1];
		
		actPos[0] = actor.pos[0];
		actPos[2] = actor.pos[1];
		
		target[0] = x;
		target[2] = y;
		if(Maths.ccwXZ(tmpVec, actPos, target) == false)
		{
			theta = -theta;
		}
		
		if((theta >= 1.0f) || (theta <= -1.0f))
		{
			actor.rotate(theta);
		}
	}
	
	/**
	 * Time since last frame in ms
	 */
	public void tick(float deltaTime) 
	{
		deltaTime *= 0.001f;
		if(actor.checkActive())
		{
			actor.move(deltaTime * SPEED_SECS, 1.0f, 0.0f);
		}
	}
}
