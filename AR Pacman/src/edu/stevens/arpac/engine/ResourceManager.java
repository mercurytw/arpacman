package edu.stevens.arpac.engine;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import edu.stevens.arpac.graphics.Mesh;
import edu.stevens.arpac.graphics.Texture;
import edu.stevens.arpac.graphics.WavefrontObjectLoader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

/**
 * A simple resource manager class. Handles Bitmaps, Meshes, and Textures
 * @author Jeff Cochran
 *
 */
public class ResourceManager 
{
	private static final String TAG = "ResourceManager";
	private Context ctx;
	private HashMap<String, Bitmap> bmpMap;
	private HashMap<String, Mesh> meshMap;
	private HashMap<String, Texture> texMap;
	private WavefrontObjectLoader loader;
	
	/**
	 * Create a new ResourceManager
	 * @param context the application context
	 */
	public ResourceManager(Context context)
	{
		ctx = context;
		bmpMap = new HashMap<String, Bitmap>();
		meshMap = new HashMap<String, Mesh>();
		texMap = new HashMap<String, Texture>();
		loader = new WavefrontObjectLoader(context);
	}
	
	/**
	 * Add in a new bitmap
	 * @param name The name that you would like the bitmap to be stored under
	 * @param filename The filename of the bitmap in the assets folder
	 */
	public void addBitmap(String name, String filename)
	{
		InputStream is;
		try {
			is = ctx.getAssets().open(filename);
		} catch (IOException e1) {
			Log.e(TAG, "Couldn't find resource " + filename);
			return;
		}
		Bitmap bitmap = BitmapFactory.decodeStream(is);
		try {
			is.close();
		} catch (IOException e){};
		
		if(bitmap == null)
		{
			Log.e(TAG, "Unable to decode resource");
			return;
		}
		
		bmpMap.put(name, bitmap);
	}
	
	/**
	 * Loads a mesh from a .obj file in the assets folder
	 * @param name The name you would like to store the mesh under
	 * @param filename The filename of the .obj file in the assets folder
	 */
	public void addMesh(String name, String filename)
	{
		Mesh m = loader.loadObject(filename);
		if(m != null)
		{
			meshMap.put(name, m);
		}
	}
	
	/**
	 * Returns the mesh or null if no mesh under that name has been stored yet
	 * @param name
	 * @return
	 */
	public Mesh getMesh(String name)
	{
		Mesh m = null;
		if((m = meshMap.get(name)) == null)
		{
			Log.w(TAG, "Couldn't find mesh named " + name);
		}
		return m;
	}
	
	/**
	 * Loads a texture from a bitmap and stores it under the name parameter
	 * @param name The name you would like to store the texture under
	 * @param filename The filename of the bitmap in the assets folder
	 */
	public void addTexture(String name, String filename)
	{
		InputStream is;
		try {
			is = ctx.getAssets().open(filename);
		} catch (IOException e1) {
			Log.e(TAG, "Couldn't find resource " + filename);
			return;
		}
		Bitmap bitmap = BitmapFactory.decodeStream(is);
		try {
			is.close();
		} catch (IOException e){};
		
		if(bitmap == null)
		{
			Log.e(TAG, "Unable to decode resource");
			return;
		}
		
		Texture tex = new Texture(bitmap);
		bitmap.recycle();
		texMap.put(name, tex);
	}
	
	/**
	 * Returns the texture stored as name or null if no texture has been stored with that name yet
	 * @param name
	 * @return
	 */
	public Texture getTexture(String name)
	{
		Texture t = null;
		if((t = texMap.get(name)) == null)
		{
			Log.w(TAG, "Couldn't find texture named " + name);
		}
		return t;
	}
	
	/**
	 * Returns the bitmap stored as name or null if no bitmap has been stored with that name yet
	 * @param name
	 * @return
	 */
	public Bitmap getBitmap(String name)
	{
		Bitmap result;
		if((result = bmpMap.get(name)) == null)
		{
			Log.w(TAG, "Couldn't find bitmap named " + name);
		}
		return result;
	}
}
