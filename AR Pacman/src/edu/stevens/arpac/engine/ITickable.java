package edu.stevens.arpac.engine;

/**
 * Represents a class that can be "ticked," i.e., updated, every frame
 * @author Jeffrey Cochran
 *
 */
public interface ITickable 
{
	public void tick(float deltaTime);
}
