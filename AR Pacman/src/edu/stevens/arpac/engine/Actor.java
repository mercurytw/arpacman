package edu.stevens.arpac.engine;

import edu.stevens.arpac.graphics.Model;
import edu.stevens.arpac.math.Maths;
import edu.stevens.arpac.math.Quaternion;

/**
 * A standard actor as in the actor/controller model. 
 * @author Jeff Cochran
 *
 */
public class Actor 
{
	public Model drawable;
	public float[] pos = new float[2];
	public float radius;
	private boolean isActive = true;
	public CollisionHandler reactor = null;
	
	/**
	 * A CollisionHandler is a class that can be anonymously defined in a controller and invoked when the 
	 * actor is collided with, like a function callback.
	 * @author Jeff Cochran
	 *
	 */
	public interface CollisionHandler
	{
		public void handleCollision();
	};
	
	/**
	 * Instantiate a new Actor at x-z position 0.0, 0.0
	 * @param m The Model this actor holds
	 * @param radius the collision radius of this actor's bounding circle
	 */
	public Actor(Model m, float radius)
	{
		drawable = m;
		pos[0] = 0.0f;
		pos[1] = 0.0f;
		this.radius = radius;
	}
	
	/**
	 * Instantiate a new Actor at x-z position 0.0, 0.0
	 * @param m The Model this actor holds
	 * @param radius the collision radius of this actor's bounding circle
	 * @param CollisionHandler A class holding the collision callback for this actor
	 */
	public Actor(Model m, float radius, CollisionHandler reactor)
	{
		drawable = m;
		pos[0] = 0.0f;
		pos[1] = 0.0f;
		this.radius = radius;
		this.reactor = reactor;
	}
	
	/**
	 * Sets the actor's position in the 2D x-z plane of world space
	 * @param x
	 * @param y
	 */
	public void setPosition(float x, float y)
	{
		pos[0] = x;
		pos[1] = y;
		drawable.setPosition(x, 0.0f, y);
	}
	
	/**
	 * Move the actor in the 2D x-z plane of world space
	 * @param x 
	 * @param y
	 */
	public void translate(float x, float y)
	{
		pos[0] += x;
		pos[1] += y;
		drawable.translate(x, 0.0f, y);
	}
	
	private float[] tmpVec = new float[4];
	/**
	 * Moves the actor with respect to it's orientation
	 * @param amount to move
	 * @param x direction relative to orientation to move in
	 * @param y direction relative to orientation to move in
	 */
	public void move(float amount, float x, float y)
	{
		tmpVec[0] = y;
		tmpVec[1] = 0.0f;
		tmpVec[2] = x;
		tmpVec[3] = 1.0f;
		
		Maths.normalize(tmpVec);
		getOrientation().rotate(tmpVec, tmpVec);
		Maths.multiply(tmpVec, amount);
		translate(tmpVec[0], tmpVec[2]);
	}
	
	/**
	 * Rotate this actor in the 2D x-z plane
	 * @param angle to rotate the actor by in degrees
	 */
	public void rotate(float angle)
	{
		drawable.rotate(angle, 0.0f, 1.0f, 0.0f);
	}
	
	/**
	 * Check to see the activity state of this actor
	 * @return a boolean indicating whether or not this actor is active
	 */
	public boolean checkActive()
	{
		return isActive;
	}
	
	/**
	 * Activate or deactivating this actor. Interpretation of activity is up to the game logic, however
	 * a deactivated actor will not be drawn and it is the intention that a deactivated actor is not to be 
	 * collided with.
	 * @param flag false to deactivate the actor, true to activate it
	 */
	public void setActive(boolean flag)
	{
		isActive = flag;
	}
	
	/**
	 * Return the current orientation of this quaternion in world space
	 * @return
	 */
	public Quaternion getOrientation()
	{
		return drawable.getOrientation();
	}
	
	/**
	 * Draw this actor's model
	 * @param vpMat the combined view & projection matrix as a column-major float-16
	 */
	public void draw(float[] vpMat)
	{
		if(isActive)
		{
			drawable.render(vpMat);
		}
	}
}
