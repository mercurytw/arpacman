package edu.stevens.arpac;

import android.hardware.Camera;
import android.os.Bundle;
import android.os.PowerManager;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.Menu;
import android.widget.FrameLayout;

public class MainActivity extends Activity 
{
	private static final String TAG = "MainActivity";
	private GameSurfaceView gameView;
	private CameraPreview cameraView;
	private CVVideoLocationProvider provider = null;
	private PowerManager.WakeLock wl = null;
	private static Context ctx = null;
	private static Resources res = null;

    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        MainActivity.ctx = getApplicationContext();
        MainActivity.res = getResources();
        setContentView(R.layout.activity_main);
        this.getActionBar().hide();
        
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "AR Pacman");
        wl.acquire();
        
        try {
			provider = new CVVideoLocationProvider(this.getApplicationContext(), "192.168.1.137", 320, 240, 32l, getResources());
		} catch (Exception e) {
			Log.e(TAG, "Failed to instantiate location provider: " + e.getMessage());
			return;
		}
        
        Camera cam = provider.getCamera();
        cameraView = new CameraPreview(this.getApplicationContext(), cam, provider);
        FrameLayout camLayout = (FrameLayout) findViewById(R.id.camera_layout);
        camLayout.addView(cameraView);
        
        gameView = new GameSurfaceView(getApplicationContext(), provider);
        FrameLayout gameLayout = (FrameLayout) findViewById(R.id.game_layout);
        gameLayout.addView(gameView);
    }
    
    public static Resources getRes()
    {
    	if(res == null)
    	{
    		Log.w(TAG, "Resources were requested before they were available");
    	}
    	return res;
    }
    
    public static Context getContext()
    {
    	if(ctx == null)
    	{
    		Log.w(TAG, "Context was requested before it was available");
    	}
    	return ctx;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
    public void onPause()
    {
    	super.onPause();
    	gameView.onPause();
    	if(provider != null)
    	{
    		provider.dispose();
    	}
    	if((wl != null) && (wl.isHeld()))
    	{
    		wl.release();
    	}
    }
    
    @Override
    public void onResume()
    {
    	super.onResume();
    	gameView.onResume();
    	if((wl != null) && (!wl.isHeld()))
    	{
    		wl.acquire();
    	}
    }

    
}
