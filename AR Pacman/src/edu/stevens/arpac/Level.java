package edu.stevens.arpac;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import android.util.JsonReader;
import android.util.Log;

import edu.stevens.arpac.engine.Actor;
import edu.stevens.arpac.engine.ITickable;
import edu.stevens.arpac.engine.ResourceManager;
import edu.stevens.arpac.graphics.Model;
import edu.stevens.arpac.graphics.ShaderProgram;

/**
 * Represents the play area. This class loads pellets and places ghosts based on positions detailed
 * in a JSON file. This class will work in two steps. First, when this class is instantiated, it will instantiate all the classes 
 * necessary to populate the level. After this class is instantiated, {@link Level#setup() setup} should be called to add everything 
 * to the draw and update lists. This lets all of the loading be deferred to the beginning of the game. Right now, this isn't set up 
 * to handle multiple levels, but it should be as simple as taking this class and making it into a state in a state machine.
 * @author Jeffrey Cochran
 *
 */
public class Level 
{
	private static final String TAG = "Level";
	private static final String ENCODING = "UTF-8";
	private static final String GHOST_POS_NAME = "ghost_spawn_pos";
	private static final String GHOST_LIST_NAME = "ghosts";
	private static final String PELLET_POS_NAME = "pellet_positions";
	
	// actor init finals
	private static final String PELLET_MESH_NAME = "cube";
	private static final String PELLET_TEXTURE_NAME = "shrug";
	// TODO: Collision radius should be a function of PTAMM scaling
	private static final float PELLET_COLLISION_RADIUS = 20.0f; 
	private static final float GHOST_COLLISION_RADIUS = 20.0f;
	
	
	// ghost property name finals
	private static final String GHOST_TYPE_PROPERTY_NAME = "type";
	private static final String GHOST_MESH_PROPERTY_NAME = "mesh";
	private static final String GHOST_TEXTURE_PROPERTY_NAME = "tex";
	
	private ArrayList<Actor> drawList;
	private ArrayList<ITickable> tickList;
	private ArrayList<Actor> actors;
	private ArrayList<ITickable> controllers;
	private float[] ghostSpawnPos = null;
	private ResourceManager resMgr;
	private ShaderProgram defaultShader;
	private Actor.CollisionHandler reactor;
	
	/**
	 * Instantiates a new level from JSON <br><br><b> *** Must be called in the GL Thread *** <br><br><b>
	 * @param jsonFilename The name of the file to containing the level JSON
	 * @param r An instance of the ResourceManager class <b>which should be populated with meshes and textures BEFORE instantiating this class</b>
	 * @param drawList An ArrayList of Actors representing the list of Actors to be drawn each frame
	 * @param tickList An ArrayList of ITickables that we will add GhostControllers to 
	 * @param reactor An {@link Actor.CollisionHandler} for the ghosts
	 */
	public Level(String jsonFilename, ResourceManager r, ArrayList<Actor> drawList, ArrayList<ITickable> tickList, Actor.CollisionHandler reactor)
	{
		this.resMgr = r;
		this.drawList = drawList;
		this.tickList = tickList;
		this.reactor = reactor;
		actors = new ArrayList<Actor>();
		controllers = new ArrayList<ITickable>();
		defaultShader = ShaderProgram.getDefaultShader();
		
		try {
			parseJson(jsonFilename);
		} catch (IOException e) {
			Log.e(TAG, "Failed to parse json");
			throw new RuntimeException();
		}
	}
	
	// XXX: Do I have to worry about ArrayList.add() being non-atomic here?
	/**
	 * Call this function to add everything to the draw and update lists. Does not need to be called from the GL thread
	 */
	public void setup( float xScale, float worldScale )
	{
		for(int i = 0; i < actors.size(); i++ ) // adjust the stuff in the world to fit the aspect ratio of the room the player is in
		{
			if( xScale < 1.0 ) // scale to the smaller of the two
			{
				actors.get(i).drawable.scale( xScale / 2.0f, xScale / 2.0f, xScale / 2.0f );
				actors.get(i).setPosition( actors.get(i).pos[0] * xScale, actors.get(i).pos[1] );
			}
			else
			{
				xScale = 1.0f / xScale;
				actors.get(i).drawable.scale( xScale / 2.0f, xScale / 2.0f, xScale / 2.0f );
				actors.get(i).setPosition( actors.get(i).pos[0], actors.get(i).pos[1] * xScale );
			}
			actors.get(i).radius *= xScale / 2.0f;
			Log.v(TAG, "Built actor with new properties: x: " + actors.get(i).pos[0] + " z:" + actors.get(i).pos[1] + " radius:" + actors.get(i).radius);
		}
		drawList.addAll(actors);
		tickList.addAll(controllers);
	}
	
	private void buildPelletFromJson(JsonReader r) throws IOException
	{
		float x,y;
		r.beginArray();
		x = (float) r.nextDouble();
		y = (float) r.nextDouble();
		r.endArray();
		
		Model pelletMdl = new Model(resMgr.getMesh(PELLET_MESH_NAME), resMgr.getTexture(PELLET_TEXTURE_NAME), defaultShader);
		Actor act = new Actor(pelletMdl, PELLET_COLLISION_RADIUS);
		act.setPosition(x, y);
		actors.add(act);
	}
	
	private void buildGhostFromJson(JsonReader r) throws IOException
	{
		if(ghostSpawnPos == null)
		{
			Log.e(TAG, "Malformed JSON: ghost spawn position was not specified before ghost list");
			throw new RuntimeException();
		}
		
		String propertyName, meshName, texName;
		meshName = texName = null;
		
		r.beginObject();
		while(r.hasNext())
		{
			propertyName = r.nextName();
			if(propertyName.equals(GHOST_TYPE_PROPERTY_NAME))
			{
				// currently unused. continue
				r.nextInt();
			}
			else if(propertyName.equals(GHOST_MESH_PROPERTY_NAME))
			{
				meshName = r.nextString();
			}
			else if(propertyName.equals(GHOST_TEXTURE_PROPERTY_NAME))
			{
				texName = r.nextString();
			}
			else
			{
				Log.w(TAG, "Encountered unknown object: \"" + propertyName +"\" ignoring...");
				r.skipValue();
			}
		}
		r.endObject();
		
		if((meshName == null) || (texName == null))
		{
			Log.e(TAG, "Malformed JSON: mesh or texture name was not specified for ghost");
			throw new RuntimeException();
		}
		
		Model ghostMdl = new Model(resMgr.getMesh(meshName), resMgr.getTexture(texName), defaultShader);
		Actor act = new Actor(ghostMdl, GHOST_COLLISION_RADIUS, reactor);
		actors.add(act);
		controllers.add(new GhostController(act, ghostSpawnPos[0], ghostSpawnPos[1]));
	}
	
	
	private void parseJson(String filename) throws IOException
	{
		String propertyName;
		JsonReader r = null;
		try {
			r = new JsonReader(new InputStreamReader(MainActivity.getContext().getAssets().open(filename), ENCODING));
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, "JSON file was not encoded in " + ENCODING);
			throw new RuntimeException();
		} 
		
		r.beginObject(); // enter enclosing object
		while(r.hasNext())
		{
			propertyName = r.nextName();
			if(propertyName.equals(GHOST_POS_NAME))
			{
				r.beginArray();
				ghostSpawnPos = new float[2];
				ghostSpawnPos[0] = (float) r.nextDouble();
				ghostSpawnPos[1] = (float) r.nextDouble();
				r.endArray();
			}
			else if(propertyName.equals(GHOST_LIST_NAME))
			{
				r.beginArray();
				while(r.hasNext())
				{
					buildGhostFromJson(r);
				}
				r.endArray();
			}
			else if(propertyName.equals(PELLET_POS_NAME))
			{
				r.beginArray();
				while(r.hasNext())
				{
					buildPelletFromJson(r);
				}
				r.endArray();
			}
			else
			{
				Log.w(TAG, "Encountered unknown object: \"" + propertyName +"\" ignoring...");
				r.skipValue();
			}
		}
		r.endObject();
		r.close();
		r = null;
	}
}
