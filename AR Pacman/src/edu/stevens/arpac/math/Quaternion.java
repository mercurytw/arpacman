package edu.stevens.arpac.math;

import java.util.Arrays;

import android.util.FloatMath;

/**
 * A class representing a Quaternion - a mathematical representation of an orientation in 3-space.
 * 
 * @author Jeff Cochran
 *
 */
public class Quaternion 
{
	//private static final String TAG = "Quaternion";
	
	/**
	 * The Zero quaternion
	 */
	public static final Quaternion ZERO = (new Quaternion(0.0f, 0.0f, 0.0f, 0.0f)).setWZero();
	/**
	 * The Identity quaternion representing no rotation
	 */
	public static final Quaternion IDENTITY = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
	
	private float[] arr = new float[4];
	private static Quaternion tmpQuat = new Quaternion(Quaternion.ZERO);
	
	/**
	 * Copy constructor
	 * @param other the quaternion to copy 
	 */
	public Quaternion(Quaternion other)
	{
		arr = Arrays.copyOf(other.arr, 4);
	}
	
	private Quaternion setWZero()
	{
		arr[3] = 0.0f;
		return this;
	}
	
	/**
	 * Instantiate a new quaternion from a rotation about an arbitrary axis
	 * @param thetaRads Angle of rotation in <b>radians</b>
	 * @param x
	 * @param y
	 * @param z
	 */
	public Quaternion(float thetaRads, float x, float y, float z)
	{
		float sin = FloatMath.sin(thetaRads * 0.5f);
		arr[0] = x * sin;
		arr[1] = y * sin;
		arr[2] = z * sin;
		arr[3] = FloatMath.cos(thetaRads * 0.5f);
	}
	
	/**
	 * Instatiate a new quaternion from a rotation about an arbitrary axis
	 * @param thetaRads Angle of rotation in <b>radians</b>
	 * @param vec
	 */
	public Quaternion(float thetaRads, float[] vec)
	{
		float sin = FloatMath.sin(thetaRads * 0.5f);
		arr[0] = vec[0] * sin;
		arr[1] = vec[1] * sin;
		arr[2] = vec[2] * sin;
		arr[3] = FloatMath.cos(thetaRads * 0.5f);
	}
	
	/**
	 * Directly set the components of this quaternion
	 * @param vx
	 * @param vy
	 * @param vz
	 * @param w
	 */
	public void setDirect(float vx, float vy, float vz, float w)
	{
		arr[0] = vx;
		arr[1] = vy;
		arr[2] = vz;
		arr[3] = w;
	}
	
	public void invertX()
	{
		arr[0] = -arr[0];
	}
	
	/**
	 * Set this quaternion to a rotation about an arbitrary axis
	 * @param thetaRads Angle of rotation in <b>radians</b>
	 * @param x
	 * @param y
	 * @param z
	 */
	public void set(float thetaRads, float x, float y, float z)
	{
		float sin = FloatMath.sin(thetaRads * 0.5f);
		arr[0] = x * sin;
		arr[1] = y * sin;
		arr[2] = z * sin;
		arr[3] = FloatMath.cos(thetaRads * 0.5f);
	}
	
	/**
	 * Set the value of this quaternion to match other quaternion
	 * @param other
	 */
	public void copy(Quaternion other)
	{
		arr[0] = other.arr[0];
		arr[1] = other.arr[1];
		arr[2] = other.arr[2];
		arr[3] = other.arr[3];
	}
	
	/**
	 * Create a quaternion from a vector. This is not a 3D rotation quaternion.
	 * @param vec
	 * @return
	 */
	public static Quaternion fromVector(float[] vec)
	{
		Quaternion quat = new Quaternion(Quaternion.ZERO);
		quat.setDirect(vec[0], vec[1], vec[2], 0.0f);
		return quat;
	}
	
	/**
	 * Create a quaternion from a vector. This is not a 3D rotation quaternion.
	 * @param vec
	 * @param the offset in the array that the vector is stored
	 * @return
	 */
	public static Quaternion fromVector(float[] vec, int offt)
	{
		Quaternion quat = new Quaternion(Quaternion.ZERO);
		quat.setDirect(vec[0 + offt], vec[1 + offt], vec[2 + offt], 0.0f);
		return quat;
	}
	
	/**
	 * Rotate a vector to this quaternion's orientation and store the result in result
	 * @param result
	 * @param vector the vector to reorient
	 */
	public void rotate(float[] result, float[] vector)
	{
		Quaternion qstar = conjugate();
		Quaternion v = Quaternion.fromVector(vector);
		
		Quaternion.multiply(tmpQuat, this, v);
		tmpQuat.multiply(qstar);
		
		result[0] = tmpQuat.arr[0];
		result[1] = tmpQuat.arr[1];
		result[2] = tmpQuat.arr[2];
		result[3] = 1.0f;
	}
	
	/**
	 * Rotate a vector to this quaternion's orientation and store the result in result
	 * @param result the float4 array in which the result will be stored
	 * @param rofft the offset in the result array to store the result in
	 * @param vector the float4 vector to reorient
	 * @param the offset in the vector array to find the vector to be reoriented
	 */
	public void rotate(float[] result, int rofft, float[] vector, int vofft)
	{
		Quaternion qstar = conjugate();
		Quaternion v = Quaternion.fromVector(vector, vofft);
		
		Quaternion.multiply(tmpQuat, this, v);
		tmpQuat.multiply(qstar);
		
		result[0 + rofft] = tmpQuat.arr[0];
		result[1 + rofft] = tmpQuat.arr[1];
		result[2 + rofft] = tmpQuat.arr[2];
		result[3 + rofft] = 1.0f;
	}
	
	/**
	 * Return the conjugate of this quaternion
	 */
	public Quaternion conjugate()
	{
		Quaternion tmp = new Quaternion(Quaternion.ZERO);
		tmp.setDirect(-arr[0], -arr[1], -arr[2], arr[3]);
		return tmp;
	}
	
	/**
	 * Vector normalize this quaternion in place to avoid float overflow & underflow
	 */
	public void normalize()
	{
		float magsq = (arr[0]*arr[0]) + (arr[1]*arr[1]) + (arr[2]*arr[2]) + (arr[3]*arr[3]);
		if(magsq == 0.0f)
		{
			return;
		}
		float mag = FloatMath.sqrt(magsq);
		arr[0] /= mag;
		arr[1] /= mag;
		arr[2] /= mag;
		arr[3] /= mag;
	}
	
	/**
	 * Rotate this quaternion's orientation about an arbitrary axis
	 * @param thetaRads Angle of rotation in <b>radians</b>
	 * @param x
	 * @param y
	 * @param z
	 */
	public void rotate(float thetaRads, float x, float y, float z)
	{
		tmpQuat.set(thetaRads, x, y, z);
		multiply(tmpQuat);
	}
	
	/**
	 * Multiply two quaternions together and store the result in a preallocated quaternion
	 * @param result The quaternion to store the result of multiplication in
	 * @param lhs the left hand side multiplicand
	 * @param rhs the right hand side multiplicand
	 */
	public static void multiply(Quaternion result, Quaternion lhs, Quaternion rhs)
	{
		result.arr[0] = (lhs.arr[3]*rhs.arr[0]) + (lhs.arr[0]*rhs.arr[3]) + (lhs.arr[1]*rhs.arr[2]) - (lhs.arr[2]*rhs.arr[1]);
		result.arr[1] = (lhs.arr[3]*rhs.arr[1]) + (lhs.arr[1]*rhs.arr[3]) + (lhs.arr[2]*rhs.arr[0]) - (lhs.arr[0]*rhs.arr[2]);
		result.arr[2] = (lhs.arr[3]*rhs.arr[2]) + (lhs.arr[2]*rhs.arr[3]) + (lhs.arr[0]*rhs.arr[1]) - (lhs.arr[1]*rhs.arr[0]);
		result.arr[3] = (lhs.arr[3]*rhs.arr[3]) - (lhs.arr[0]*rhs.arr[0]) - (lhs.arr[1]*rhs.arr[1]) - (lhs.arr[2]*rhs.arr[2]);
	}
	
	private float[] tmpVec = new float[4];
	/**
	 * Right-multiply this quaternion by another and store the result in this quaternion.
	 * @param rhs the right hand side multiplicand
	 */
	public void multiply(Quaternion rhs)
	{
		tmpVec[0] = (arr[3]*rhs.arr[0]) + (arr[0]*rhs.arr[3]) + (arr[1]*rhs.arr[2]) - (arr[2]*rhs.arr[1]);
		tmpVec[1] = (arr[3]*rhs.arr[1]) + (arr[1]*rhs.arr[3]) + (arr[2]*rhs.arr[0]) - (arr[0]*rhs.arr[2]);
		tmpVec[2] = (arr[3]*rhs.arr[2]) + (arr[2]*rhs.arr[3]) + (arr[0]*rhs.arr[1]) - (arr[1]*rhs.arr[0]);
		tmpVec[3] = (arr[3]*rhs.arr[3]) - (arr[0]*rhs.arr[0]) - (arr[1]*rhs.arr[1]) - (arr[2]*rhs.arr[2]);
		
		arr[0] = tmpVec[0];
		arr[1] = tmpVec[1];
		arr[2] = tmpVec[2];
		arr[3] = tmpVec[3];
	}
	
	/**
	 * Convert the orientation of the quaternion into a column-major rotation matrix
	 * @param mat The preallocated float array to store the rotation matrix in
	 * @param rotMatOnly if true, then this function will create a 3x3 rotation matrix, otherwise 
	 *                   it will produce a rotated 4x4 identity matrix
	 * @param offt the offset into the matrix to store the result
	 */
	public void toRotMat(float[] mat, boolean rotMatOnly, int offt)
	{
		if(rotMatOnly)
		{
			mat[0+offt] = 1 - (2 * arr[1] * arr[1]) - (2 * arr[2] * arr[2]);
			mat[1+offt] = (2 * arr[0] * arr[1]) + (2 * arr[3] * arr[2]);
			mat[2+offt] = (2 * arr[0] * arr[2]) - (2 * arr[3] * arr[1]);
			
			mat[3+offt] = (2 * arr[0] * arr[1]) - (2 * arr[3] * arr[2]);
			mat[4+offt] = 1 - (2 * arr[0] * arr[0]) - (2 * arr[2] * arr[2]);
			mat[5+offt] = (2 * arr[1] * arr[2]) + (2 * arr[3] * arr[0]);
			
			mat[6+offt] = (2 * arr[0] * arr[2]) + (2 * arr[3] * arr[1]);
			mat[7+offt] = (2 * arr[1] * arr[2]) - (2 * arr[3] * arr[0]);
			mat[8+offt] = 1 - (2 * arr[0] * arr[0]) - (2 * arr[1] * arr[1]);
		}
		else
		{
			mat[0+offt] = 1 - (2 * arr[1] * arr[1]) - (2 * arr[2] * arr[2]);
			mat[1+offt] = (2 * arr[0] * arr[1]) + (2 * arr[3] * arr[2]);
			mat[2+offt] = (2 * arr[0] * arr[2]) - (2 * arr[3] * arr[1]);
			mat[3+offt] = 0.0f;

			mat[4+offt] = (2 * arr[0] * arr[1]) - (2 * arr[3] * arr[2]);
			mat[5+offt] = 1 - (2 * arr[0] * arr[0]) - (2 * arr[2] * arr[2]);
			mat[6+offt] = (2 * arr[1] * arr[2]) + (2 * arr[3] * arr[0]);
			mat[7+offt] = 0.0f;

			mat[8+offt] = (2 * arr[0] * arr[2]) + (2 * arr[3] * arr[1]);
			mat[9+offt] = (2 * arr[1] * arr[2]) - (2 * arr[3] * arr[0]);
			mat[10+offt] = 1 - (2 * arr[0] * arr[0]) - (2 * arr[1] * arr[1]);
			mat[11+offt] = 0.0f;

			mat[12+offt] = 0.0f;
			mat[13+offt] = 0.0f;
			mat[14+offt] = 0.0f;
			mat[15+offt] = 1.0f;
		}
	}
	
	/*private static int sign(float val)
	{
		return ( val >= 0.0f ) ? 1 : -1;
	}*/
	
	// from http://www.cg.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/teche52.html
	/**
	 * Construct a new Quaternion from a column-major float3x3 rotation matrix
	 * @param mat the column-major float 3x3 rotation matrix
	 * @return the new Quaternion
	 */
	// XXX: Doesn't work
	/*public static Quaternion fromRotMat(float[] mat)
	{
		float q0 = ( mat[0] + mat[4] + mat[8] + 1.0f ) / 4.0f;
		float q1 = ( mat[0] - mat[4] - mat[8] + 1.0f ) / 4.0f;
		float q2 = (-mat[0] + mat[4] - mat[8] + 1.0f ) / 4.0f;
		float q3 = (-mat[0] - mat[4] + mat[8] + 1.0f ) / 4.0f;
		if(q0 < 0.0f) q0 = 0.0f;
		if(q1 < 0.0f) q1 = 0.0f;
		if(q2 < 0.0f) q2 = 0.0f;
		if(q3 < 0.0f) q3 = 0.0f;
		q0 = FloatMath.sqrt(q0);
		q1 = FloatMath.sqrt(q1);
		q2 = FloatMath.sqrt(q2);
		q3 = FloatMath.sqrt(q3);
		if(q0 >= q1 && q0 >= q2 && q0 >= q3) {
		    q0 *= +1.0f;
		    q1 *= sign(mat[5] - mat[7]);
		    q2 *= sign(mat[6] - mat[2]);
		    q3 *= sign(mat[1] - mat[3]);
		} else if(q1 >= q0 && q1 >= q2 && q1 >= q3) {
		    q0 *= sign(mat[5] - mat[7]);
		    q1 *= +1.0f;
		    q2 *= sign(mat[1] + mat[3]);
		    q3 *= sign(mat[6] + mat[2]);
		} else if(q2 >= q0 && q2 >= q1 && q2 >= q3) {
		    q0 *= sign(mat[6] - mat[2]);
		    q1 *= sign(mat[1] + mat[3]);
		    q2 *= +1.0f;
		    q3 *= sign(mat[5] + mat[7]);
		} else if(q3 >= q0 && q3 >= q1 && q3 >= q2) {
		    q0 *= sign(mat[1] - mat[3]);
		    q1 *= sign(mat[2] + mat[6]);
		    q2 *= sign(mat[5] + mat[7]);
		    q3 *= +1.0f;
		} else {
			Log.e(TAG, "Failed to convert rot mat to quaternion!");
			return null;
		}
		Quaternion q = new Quaternion(Quaternion.ZERO);
		q.setDirect( q0, q1, q2, q3 );
		q.normalize();
		return q;
	} */
}
