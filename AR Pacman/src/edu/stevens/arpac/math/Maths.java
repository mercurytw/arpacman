package edu.stevens.arpac.math;

import java.util.Arrays;

import android.util.Log;

/**
 * A run-of-the-mill static math class. Math functions will be implemented as they are needed, 
 * so expect this class to grow with time.
 * @author Jeff Cochran
 *
 */
public class Maths 
{
	public static final String TAG = "Math";
	/**
	 * Dot product between two homogenous vectors of arbitrary dimensionality
	 * @param vec1
	 * @param vec2
	 * @return
	 */
	public static float dotProduct(float[] vec1, float[] vec2)
	{
		float result = 0.0f;
		
		if(vec1.length != vec2.length)
		{
			Log.e(TAG, "DotProduct: vectors of different dimensionality");
			throw new IllegalArgumentException(TAG + " DotProduct: vectors of different dimensionality");
		}
		
		for(int i = 0; i < vec1.length-1; i++)
		{
			result += vec1[i] * vec2[i];
		}
		return result;
	}
	
	/**
	 * Crosses two float-3 vectors
	 * @param lhs the lefthand-side vector
	 * @param offt1 the offset into the lhs array in which the lhs vector begins
	 * @param rhs the righthand-side vector
	 * @param offt2 the offset into the rhs array in which the rhs vector begins
	 * @param result the array in which the resulting vector will be stored
	 * @param offt3 the offset into the result array where the result will be stored
	 **/
	public static void crossProduct(float[] lhs, int offt1, float[] rhs, int offt2, float[] result, int offt3 )
	{
		result[0 + offt3] = (lhs[1 + offt1] * rhs[2 + offt2]) - (lhs[2 + offt1] * rhs[1 + offt2]);
		result[1 + offt3] = (lhs[2 + offt1] * rhs[0 + offt2]) - (lhs[0 + offt1] * rhs[2 + offt2]);
		result[2 + offt3] = (lhs[0 + offt1] * rhs[1 + offt2]) - (lhs[1 + offt1] * rhs[0 + offt2]);
	}
	
	/**
	 * Magnitude-squared of a homogenous vector of arbitrary dimensionality
	 * @param vec
	 * @return
	 */
	public static float magSq(float[] vec)
	{
		float res = 0.0f;
		for(int i = 0; i < vec.length -1 ; i++)
		{
			res += vec[i] * vec[i];
		}
		
		return res;
	}
	
	/**
	 * Magnitude-squared of a homogenous vector of length 4
	 * @param vec the float4 buffer where the vector is stored
	 * @param offt the index into the buffer where the vector begins
	 * @return
	 */
	public static float magSqFloat4(float[] vec, int offt)
	{
		float res = 0.0f;
		for(int i = 0; i < 3; i++)
		{
			res += vec[i+offt] * vec[i+offt];
		}
		
		return res;
	}
	
	/**
	 * Magnitude of a homogenous vector of arbitrary dimensionality
	 * @param vec
	 * @return
	 */
	public static float magnitude(float[] vec)
	{
		return (float) Math.sqrt((double) magSq(vec));
	}
	
	
	/**
	 * Magnitude of a homogenous vector of length 4
	 * @param vec the float4 buffer where the vector is stored
	 * @param offt the index into the buffer where the vector begins
	 */
	public static float magnitude(float[] vec, int index)
	{
		return (float) Math.sqrt((double)magSqFloat4(vec, index));
	}

	
	/**
	 * Angle between two vectors of equal arbitrary dimensionality
	 * @param vec1
	 * @param vec2
	 * @return the angle between the two, in radians
	 */
	public static float angleBetween(float[] vec1, float[] vec2)
	{
		float result = dotProduct(vec1, vec2)/(magnitude(vec1) * magnitude(vec2));
		result = (float) Math.acos(result);
		return result;
	}
	
	/**
	 * Are points a b and c in ccw order about the Y axis?
	 * @param a
	 * @param b
	 * @param c
	 * @return
	 */
	public static boolean ccwXZ( float[] a, float[] b, float[] c )
	{
		return(((c[2]-a[2])*(b[0]-a[0])) > ((b[2]-a[2])*(c[0]-a[0])));
	}
	
	/**
	 * Adds vecs 1 and 2 and stores the result in vec1
	 * @param vec1
	 * @param vec2
	 */
	public static void addInPlace(float[] vec1, float[] vec2)
	{
		if(vec1.length != vec2.length)
		{
			Log.e(TAG, "addInPlace: vec1 and vec2 of different size!");
			throw new IllegalArgumentException(TAG + " addInPlace: vec1 and vec2 of different size!");
		}
		for(int i = 0; i < vec1.length -1; i++)
		{
			vec1[i] += vec2[i];
		}
	}
	
	/**
	 * Normalizes a vector in place
	 * @param vec4 a homogeneous float-4
	 */
	public static void normalize(float[] vec4)
	{
		float mag = magnitude(vec4);
		vec4[0] /= mag;
		vec4[1] /= mag;
		vec4[2] /= mag;
		vec4[3] = 1.0f;
	}
	
	public static void normalize(float[] vec4, int offt)
	{
		float mag = magnitude(vec4, offt);
		vec4[0] /= mag;
		vec4[1] /= mag;
		vec4[2] /= mag;
		vec4[3] = 1.0f;
	}
	
	/**
	 * Multiplies a homogeneous vector by a scalar in place
	 * @param vec4
	 * @param scalar
	 */
	public static void multiply(float[] vec4, float scalar)
	{
		vec4[0] *= scalar;
		vec4[1] *= scalar;
		vec4[2] *= scalar;
	}
	
	/**
	 * Like android's multiplyMM, except that we're willing to multiply 3x3's as well.
	 * Column-major
	 * @param result the 3x3 or 4x4 matrix to hold the result in
	 * @param lhs the 3x3 or 4x4 left hand side matrix
	 * @param rhs the 3x3 or 4x4 left hand side matrix
	 */
	public static void multiplyMM(float[] result, float[] lhs, float[] rhs)
	{
		if((result.length != lhs.length) || (lhs.length != rhs.length) || ((rhs.length != 9) && (rhs.length != 16)))  
		{
			throw new IllegalArgumentException();
		}
		
		int wh = lhs.length;
		int h = (wh == 9) ? 3 : 4;
		int n, i, j, k;
		n = i = j = k = 0;

		Arrays.fill(result, 0.0f);
		while(n < (wh * h))
		{
			i = n % wh;
			j = n / h;
			k = (n % h) + ((n / wh) * h);
			result[k] += lhs[i]*rhs[j];
			n++;
		}
	}
}
