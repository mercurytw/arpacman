package edu.stevens.arpac.math;

import android.util.Log;

/**
 * Deprecated and marked for deletion
 * 
 * rationale: this class provides no functionality significantly more useful than a float array, 
 * and vector math is provideded in the Maths class
 * 
 * @author Jeff Cochran
 *
 */
@Deprecated
public class Vector3 
{
	private static final String TAG = "Vector3";
	public float x,y,z;
	private float[] vec4 = new float[4];
	public Vector3()
	{
		x = y = z = 0.0f;
		vec4[3] = 1.0f;
	}
	
	public Vector3(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		vec4[3] = 1.0f;
	}
	
	public float[] getFloat4()
	{
		vec4[0] = x;
		vec4[1] = y;
		vec4[2] = z;
		return vec4;
	}
	
	public void set(float x, float y, float z)
	{
		vec4[0] = x;
		vec4[1] = y;
		vec4[2] = z;
	}
	
	public static void set(float[] vec, float x, float y, float z)
	{
		if(vec.length < 3)
		{
			Log.e(TAG, "Passed in a float[] of length less than 3");
			return;
		}
		vec[0] = x;
		vec[1] = y;
		vec[2] = z;
	}
}
