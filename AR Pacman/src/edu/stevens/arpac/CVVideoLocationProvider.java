package edu.stevens.arpac;

import android.content.Context;
import android.content.res.Resources;
import android.hardware.Camera;
import android.os.Handler;
import android.util.Log;
import android.opengl.Matrix;

import edu.stevens.arpac.math.Maths;
import edu.stevens.arpac.math.Quaternion;
import edu.stevens.arpac.webclient.PtammLocationProvider;

/**
 * An adapter class for presenting the {@link PtammLocationProvider}'s functionality to the platform-agnostic 
 * Pacman project. Also handles the camera throughout the app lifecycle and presenting the PtammLocationProvider 
 * with a running camera.
 * @author Jeff Cochran
 *
 */
public class CVVideoLocationProvider implements Camera.PreviewCallback
{
	private static final String TAG = "CVVideoLocationProvider";
	private boolean calculatingBasisChange = true;
	private Quaternion ptammOrientationXform = null;
	private float[] ptammCenterPos = null;
	private float[] ptammXaxis = null;
	private float[] ptammYaxis = null;
	private float[] ptammZaxis = null;
	private float[] ptammXformMatrix;
	private float[] ptammXaxisPrior = null;
	private float[] ptammZaxisPrior = null;
	public PtammLocationProvider locProv;
	public Camera cam;
	private boolean isTorndown = false;
	private boolean isPreviewStarted = false;
	private Handler handler;
	
	public float worldAspect;
	public float worldScale;
	
	/**
	 * See {@link PtammLocationProvider}'s constructor for details.
	 * @param context The Application context
	 * @param serverAddr An IPv4 string representing the address of the PTAMM server
	 * @param previewWidth The width of the preview image
	 * @param previewHeight The height of the preview image
	 * @param framerateMillis The minimum frequency of frames in milliseconds.
	 * @param res A Resources instance from the main activity 
	 * @throws Exception If we couldn't find and lock a backfacing camera.
	 */
	public CVVideoLocationProvider(Context context, String serverAddr, int previewWidth, int previewHeight, long framerateMillis, Resources res) throws Exception
	{
		locProv = new PtammLocationProvider(context, serverAddr, previewWidth, previewHeight, framerateMillis, res, false);
		cam = getCameraInstance();
		if(cam == null)
		{
			Log.e(TAG, "UNABLE TO ACQUIRE SATISFACTORY CAMERA");
			throw new RuntimeException("couldn't get camera");
		}
		
		Camera.Parameters params = cam.getParameters();
		params.setPreviewSize( previewWidth, previewHeight );
		cam.setParameters(params);
		
		cam.setPreviewCallbackWithBuffer(this);

		if(!locProv.setupCamera(cam))
		{
			Log.e(TAG, "Android device incompatible with webclient library");
			this.dispose();
			return;
		}
	}
	
	/**
	 * Inform the camera to begin to start producing preview frames. When using this library, this function
	 * should be called instead of calling cam.startPreview() directly.
	 */
	public void start()
	{
		handler = new Handler();
		Runnable r = new Runnable() {
			public void run() 
			{
				cam.startPreview();
				isPreviewStarted = true;
			}
		};
		handler.postDelayed(r, 5000l);
	}
	
	/**
	 * Get the hardware camera that will be used by this library
	 * @return a hardware.Camera
	 */
	public Camera getCamera()
	{
		return cam;
	}
	
	/** A safe way to get an instance of the Camera object. */
    private static Camera getCameraInstance()
    {
        Camera c = null;
        Camera.CameraInfo info = new Camera.CameraInfo();
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) 
        {
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) 
            {
            	try 
            	{
            		c = Camera.open(i);                 
            	}
            	catch (Exception e)
                {
                	// Camera is not available (in use or does not exist)
                	Log.e(TAG, "Could not access camera: " + e.getMessage());
                }
            }    
        }
        return c; // returns null if camera is unavailable
    }
	
    /**
     * Get the latest processed video frame. The inbuf parameter should be set to the appropriate size for
     * the preview size. May block. Returns video in ARGB
     * @param inbuf A preallocated byte[] to which the latest video frame will be copied
     * @return true if the frame was copied successfully or false if no frames were available
     */
	public boolean getVideoFrame(byte[] inbuf) {
		return locProv.getRgba8888(inbuf);
	}
	
	/**
	 * Get the latest processed video frame. The inbuf parameter should be set to the appropriate size for
     * the preview size. May block. Returns video in BGRA
     * @param inbuf A preallocated int[] to which the latest video frame will be copied
     * @return true if the frame was copied successfully or false if no frames were available
	 */
	public boolean getVideoFrame(int[] inbuf) {
		return locProv.getRgba8888(inbuf);
	}
	
	private static float[] playerPos = new float[3];
	/**
	 * Once PTAMM is initialized to get the last reported player position. This function may block
	 * @return a float-3 representing player position
	 */
	public float[] getPlayerPosition() {
		if(isTorndown) { return null; }
		
		edu.stevens.arpac.webclient.Vector3 vec = locProv.getPlayerPosition();
		
		if( vec == null ) { return null; }
		/*if( pos == null )
		{
			pos = new Vector3(vec.x, vec.y, vec.z);
		}
		else
		{
			pos.set(vec.x, vec.y, vec.z);
		}*/
		playerPos[0] = vec.x;
		playerPos[1] = vec.y;
		playerPos[2] = vec.z;
		return playerPos;
	}
	
	private Quaternion playerOrientation = new Quaternion(Quaternion.IDENTITY);
	private float[] tmpVec = new float[4];
	/**
	 * Called once PTAMM is initialized to get the last reported player orientation. This function may block
	 * @return a quaternion representing player orientation or null if called in an invalid state
	 */
	public Quaternion getPlayerOrientation(boolean dumpLie)
	{
		float theta = 0;
		if(isTorndown) { return null; }
		
		edu.stevens.arpac.webclient.Vector3 vec = locProv.getPlayerOrientation();
		
		if( vec == null ) { return null; }
		tmpVec[0] = vec.x;
		tmpVec[1] = vec.y;
		tmpVec[2] = vec.z;
		tmpVec[3] = 1.0f;
		theta = Maths.magnitude(tmpVec);
		tmpVec[0] /= theta;
		tmpVec[1] /= theta;
		tmpVec[2] /= theta;
		if(dumpLie)
		{
			Log.d(TAG, "Got lie: " + vec.toString());
			Log.d(TAG, "  theta: " + theta);
			Log.d(TAG, "   axis:  { " + tmpVec[0] + ", " + tmpVec[1] + ", " + tmpVec[2] + "}");  
		}
		
		playerOrientation.set(theta, tmpVec[0], tmpVec[1], tmpVec[2]);
		return playerOrientation;
	}
	
	/**
	 * Takes a Special Orthogonal Lie and converts it into an quaternion
	 * @param lieBuff a float[] buffer holding the lie
	 * @param offset the offset into the buffer where the lie lies
	 * @return a Quaternion representing player orientation or null if called in an invalid state
	 */
	public Quaternion getQuatFromLie(float[] lieBuff, int offset)
	{
		float theta = 0;
		if(isTorndown) { return null; }
		
		if( lieBuff == null ) { return null; }
		theta = Maths.magnitude(lieBuff, offset);
		tmpVec[0] = lieBuff[offset + 0] / theta;
		tmpVec[1] = lieBuff[offset + 1] / theta;
		tmpVec[2] = lieBuff[offset + 2] / theta;
		
		playerOrientation.set(theta, tmpVec[0], tmpVec[1], tmpVec[2]);
		return playerOrientation;
	}
	
	private boolean pauseOnce = true;
	// TODO: What should we really do here? 
	// Can we really recover from a pause without reinitializing serverside?
	// Actually, I think so.
	/**
	 * To be placed in the overridden {@link com.badlogic.gdx.ApplicationListener#pause} method
	 */
	public void pause()
	{
		if(!pauseOnce)
		{ return; }
		Log.d(TAG, "Paused");
		if(isPreviewStarted)
		{
			cam.stopPreview();
		}
		cam.unlock();
		pauseOnce = false;
	}
	
	/**
	 * To be placed in the overridden {@link com.badlogic.gdx.ApplicationListener#resume} method
	 */
	public void resume()
	{
		cam.lock();
		if(isPreviewStarted)
		{
			cam.startPreview();
		}
		pauseOnce = true;
	}

	public void dispose() 
	{
		if(!isTorndown)
		{
			isTorndown = true;
			locProv.teardown();
			pause();
			isPreviewStarted = false;
		}
	}

	/**
	 * preview frame handler 
	 */
	public void onPreviewFrame(byte[] data, Camera camera) 
	{
		if(!isTorndown)
		{
			locProv.processFrame();
		}
	}
	
	private Quaternion tmp = new Quaternion(Quaternion.ZERO);
	
	/**
	 * Takes a position and orientation and transforms it from PTAMM coordinates to world coordinates in place
	 * @param pos A float-4 representing camera position in PTAMM coordinates. May be null for no pos operation
	 * @param quat A quaternion representing camera orientation relative to PTAMM coordinates. May be null for no orientation operation
	 */
	public void transformPose(float[] resultVec, int offt, float[] pos, Quaternion quat ) 
	{
		if(calculatingBasisChange)
		{
			Log.e(TAG, "transformPose() called in an invalid state");
			return;
		}
		if(pos != null)
		{
			Matrix.multiplyMV(resultVec, offt, ptammXformMatrix, 0, pos, 0);
		}
		if(quat != null)
		{
			Quaternion.multiply(tmp, quat, ptammOrientationXform);
			tmp.invertX();
			quat.copy(tmp);
		}
	}

	/**
	 * Takes a position and transforms it from world coordinates to PTAMM coordinates in place
	 * @param pos A float-4 representing a point in world coordinates
	 */
	public void transformPointWorldToPTAMM(float[] pos)
	{
		if(calculatingBasisChange)
		{
			Log.e(TAG, "transformPoint() called in an invalid state");
			return;
		}
		Matrix.multiplyMV(pos, 0, ptammXformMatrix, 0, pos, 0);
	}
	
	/**
	 * Solves a system of linear equations that represent the transformation between our world coordinate system
	 * and the arbitrary PTAMM coordinate system
	 * @param a point 1 x
	 * @param b point 1 y
	 * @param c point 2 x
	 * @param d point 2 y
	 */
/*	private void solveSystem(float a, float b, float c, float d)
	{
		float t, beta;
		float p = ptammCenterPos[0];
		float r = ptammCornerPos[0];
		
		t = (c - a) / (r - p);
		beta = a - (t * p);
		
		ptammXformMultipliers[0] = t;
		ptammXformOffsets[0] = beta;
		
		p = ptammCenterPos[1];
		r = ptammCornerPos[1];
		
		t = (d - b) / (r - p);
		beta = b - (t * p);
		
		ptammXformMultipliers[1] = t;
		ptammXformOffsets[1] = beta;
	}*/
	
	private String formatMat( float mat[], int offt )
	{
		String s = "[";
		for( int i = 0; i < 4; i++ )
		{
			if( i != 0 )
			{
				s += "\n";
			}
			s += "[ " + mat[ offt + i ] + ", " + mat[ offt + i + 4 ] + ", " + mat[ offt + i + 8 ] + ", " + mat[ offt + i + 12 ] + "]"; 
		}
		s += "]";
		return s;
	}
	
	private void recordCenterPoint()
	{
		float[] pos = getPlayerPosition();
		if(pos != null)
		{	
			ptammCenterPos = new float[4];
			ptammCenterPos[0] = pos[0];
			ptammCenterPos[1] = pos[1];
			ptammCenterPos[2] = pos[2];
			ptammCenterPos[3] = 0.0f;
			Log.d(TAG, "Got center position");
			Log.d(TAG, "PTAMM center pos is: {" + ptammCenterPos[0] + ", " + ptammCenterPos[1] + ", " + ptammCenterPos[2] + "}");
			
			ptammOrientationXform = getPlayerOrientation(false);
			ptammOrientationXform.normalize();
			ptammOrientationXform = ptammOrientationXform.conjugate();
			ptammOrientationXform.normalize();
			
			Log.d(TAG, "Got orientation transform");
		}
	}
	
	private void recordXAxis()
	{
		float[] pos = getPlayerPosition();
		if(pos != null)
		{
			ptammXaxisPrior = new float[4];
			ptammXaxis = new float[4];
			ptammXaxisPrior[0] = pos[0]   - ptammCenterPos[0];
			ptammXaxisPrior[1] = pos[1]  - ptammCenterPos[1];
			ptammXaxisPrior[2] = pos[2]  - ptammCenterPos[2];
			ptammXaxisPrior[3] = 0.0f;
			
			ptammXaxis[0] = ptammXaxisPrior[0];
			ptammXaxis[1] = ptammXaxisPrior[1];
			ptammXaxis[2] = ptammXaxisPrior[2];
			ptammXaxis[3] = 0.0f;
			
			Maths.normalize(ptammXaxis, 0);
			Log.d(TAG, "Got X axis");
		}
	}
	
	private void recordZAxis(float pos[])
	{
		ptammZaxisPrior = new float[4];
		ptammZaxis = new float[4];
		ptammZaxisPrior[0] = -(pos[0] - ptammCenterPos[0]);
		ptammZaxisPrior[1] = -(pos[1] - ptammCenterPos[1]);
		ptammZaxisPrior[2] = -(pos[2] - ptammCenterPos[2]);
		ptammZaxisPrior[3] = 0.0f;
		
		
		ptammZaxis[0] = ptammZaxisPrior[0];
		ptammZaxis[1] = ptammZaxisPrior[1];
		ptammZaxis[2] = ptammZaxisPrior[2];
		ptammZaxis[3] = 0.0f;
		
		Maths.normalize(ptammZaxis, 0);
		
		Log.d(TAG, "Got Z axis");
	}
	
	private void calculateYAxis()
	{
		ptammYaxis = new float[8];
		Maths.crossProduct(ptammZaxis, 0, ptammXaxis, 0, ptammYaxis, 0);
		ptammYaxis[3] = 0.0f;
		
		Maths.normalize(ptammYaxis, 0);
		
		// next, let's fix the z-axis to make sure it's totally perpendicular to the x-axis
		// for that, simply one more cross product
		Maths.crossProduct(ptammXaxis, 0, ptammYaxis, 0, ptammZaxis, 0);
		Maths.normalize(ptammZaxis, 0);
	}
	
	private long initTime = 0l;
	/**
	 * This must be called three times. It will perform the necessary calculations to allow usage of the {@link CVVideoLocationProvider#transformPose(float[], Quaternion) transformPose()} 
	 * method. The system may need to be called more than three times if the orientation or position is not available when
	 * this method is called.
	 * @param topRightX The extent of the world coordinates in the x
	 * @param topRightY The extent of the world coordinates in the z
	 * @return
	 */
	public boolean calculateBasisChange(float maxX, float maxZ)
	{
		if(!locProv.isPtammInitialized())
		{
			return false; // by returning false, I should ignore any more motion events from this touch
		}
		if(calculatingBasisChange)
		{
			if((System.currentTimeMillis() - initTime) < 1000l) // cool to avoid accidental doubletap
			{
				return false;
			}
			if(ptammCenterPos == null)
			{
				recordCenterPoint();
			}
			else if( ptammXaxis == null )
			{
				recordXAxis();
			}
			else if( ptammZaxis == null )
			{
				float[] pos = getPlayerPosition();
				if(pos != null)
				{
					recordZAxis(pos);
					
					calculateYAxis();
					
					float translationComponent[] = new float[4];
					
					ptammXformMatrix = new float[16];
					ptammXformMatrix[0] = ptammXaxis[0];
					ptammXformMatrix[1] = ptammYaxis[0];
					ptammXformMatrix[2] = ptammZaxis[0];
					ptammXformMatrix[3] = 0.0f;
					ptammXformMatrix[4] = ptammXaxis[1];
					ptammXformMatrix[5] = ptammYaxis[1];
					ptammXformMatrix[6] = ptammZaxis[1];
					ptammXformMatrix[7] = 0.0f;
					ptammXformMatrix[8] = ptammXaxis[2];
					ptammXformMatrix[9] = ptammYaxis[2];
					ptammXformMatrix[10] = ptammZaxis[2];
					ptammXformMatrix[11] = 0.0f; 
					ptammXformMatrix[12] = 0.0f;
					ptammXformMatrix[13] = 0.0f;
					ptammXformMatrix[14] = 0.0f;
					ptammXformMatrix[15] = 1.0f;
					
					float xMag = Maths.magnitude( ptammXaxisPrior, 0 );
					float zMag = Maths.magnitude( ptammZaxisPrior, 0 );
					
					this.worldAspect = xMag / zMag;
					Log.d(TAG, "World aspect ratio is " + worldAspect);
					
					worldScale = maxZ / zMag;
					
					Matrix.scaleM(ptammXformMatrix, 0, worldScale, worldScale, worldScale); // scale THEN set the origin
					
					Matrix.multiplyMV(translationComponent, 0, ptammXformMatrix, 0, ptammCenterPos, 0);
					
					Log.d(TAG, "After rotation & scaling: {" + translationComponent[0] + ", " + translationComponent[1] + ", " + translationComponent[2] + "}");
					
					ptammXformMatrix[12] = -translationComponent[0];
					ptammXformMatrix[13] = -translationComponent[1];
					ptammXformMatrix[14] = -translationComponent[2];
					
					Log.d(TAG, "Got matrix: " + formatMat( ptammXformMatrix, 0 ) );
					
					calculatingBasisChange = false;
				}
			}
			return false;
		}
		return true;
	}
}
