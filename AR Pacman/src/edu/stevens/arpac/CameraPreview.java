package edu.stevens.arpac;

import java.io.IOException;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/** A basic Camera preview class */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback 
{
    @SuppressWarnings("unused")
	private static final float MILLIS_TO_SECONDS = 0.001f;
    private static final int WIDTH = 320;
    private static final int HEIGHT = 240;
	private static final String TAG = "CameraPreview";
    
    private CVVideoLocationProvider provider;
    private SurfaceHolder mHolder;
    private Camera mCamera;
 
    private boolean isStopped = false;
    public byte[] previewBuffer;

    @SuppressWarnings("deprecation")
	public CameraPreview(Context context, Camera camera, CVVideoLocationProvider provider) {
        super(context);
        mCamera = camera;
        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        
        int size = (HEIGHT * WIDTH) + ((HEIGHT / 2) * (WIDTH / 2) * 2);
        previewBuffer = new byte[size];
        this.provider = provider;
    }
    
    public void stop()
    {
    	if(!isStopped)
    	{
    		isStopped = true;
    	}
    }

    public void surfaceCreated(SurfaceHolder holder) {
    	Log.d(TAG, "Surface created");
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            Log.d(TAG, "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) 
    {    
    	stop();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.
    	Log.d(TAG, "Surface changed");
        if (mHolder.getSurface() == null){
          // preview surface does not exist
          return;
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try 
        {
        	mCamera.setPreviewDisplay(mHolder);
        	Camera.Parameters params = mCamera.getParameters();
        	params.setPreviewSize( 320, 240 );
        	mCamera.setParameters(params);
        } catch (IOException e){
        	Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
        provider.start();
    }
}