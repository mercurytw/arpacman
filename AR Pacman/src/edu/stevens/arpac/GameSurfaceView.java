package edu.stevens.arpac;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

public class GameSurfaceView extends GLSurfaceView
{
	@SuppressWarnings("unused")
	private static final String TAG = "GameSurfaceView";
	private PacRenderer renderer; 
	
	public GameSurfaceView(Context context, CVVideoLocationProvider provider) 
	{
		super(context);
		setEGLContextClientVersion(2);
		this.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
		getHolder().setFormat(PixelFormat.TRANSLUCENT);
		renderer = new PacRenderer(context, provider);
		this.setRenderer(renderer);
		this.setZOrderOnTop(true);
		this.setOnTouchListener(renderer);
	}
	
	public GameSurfaceView(Context context, AttributeSet attrs, CVVideoLocationProvider provider)
	{
		super(context, attrs);
		setEGLContextClientVersion(2);
		this.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
		getHolder().setFormat(PixelFormat.TRANSLUCENT);
		renderer = new PacRenderer(context, provider);
		this.setRenderer(renderer);
		this.setZOrderOnTop(true);
		this.setOnTouchListener(renderer);
	}

}
