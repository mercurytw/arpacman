package edu.stevens.arpac.graphics;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import android.content.Context;
import android.util.Log;


/**
 * Lean & mean wavefront object loader. Currently only cares about verticies and texture coordinates. all other stuff is
 * ignored (for now).
 * @author Jeff Cochran
 *
 */
public class WavefrontObjectLoader 
{
	private static final String TAG = "WavefrontObjectLoader";
	private static final int CARRIAGE_RETURN = 13;
	private char[] buff = new char[256];
	private int numOfVerticies = 0;
	private int bufPos = 0;
	private ArrayList<Float> verts = new ArrayList<Float>(4096);
	private ArrayList<Float> texCoords = new ArrayList<Float>(4096);
	private ArrayList<Integer> faces = new ArrayList<Integer>(4096);
	private Context ctx;
	public WavefrontObjectLoader(Context ctx)
	{
		this.ctx = ctx;
	}
	
	public Mesh loadObject(String filename)
	{
		Mesh result = null;
		char c;
		numOfVerticies = 0;
		boolean go = true;
		boolean fail = false;
		InputStreamReader is;
		bufPos = 0;
		try {
			is = new InputStreamReader(ctx.getAssets().open(filename));
		} catch (IOException e1) {
			Log.e(TAG, "Couldn't find resource " + filename);
			return null;
		}
		Arrays.fill(buff, '\0');
		try 
		{
			int val;
			while(go)
			{
				val = is.read();
				if(val == -1) // EOF
				{
					go = false;
					processLine();
					bufPos = 0;
				}
				if((c = (char) val) == CARRIAGE_RETURN)
				{
					is.read(); // pass the line feed. We make the simplifying assumption that LF's follow CR's always
					processLine();
					bufPos = 0;
				}
				else if(c == '\n')
				{
					processLine();
					bufPos = 0;
				}
				else
				{
					buff[bufPos++] = c;
					if(bufPos == 256)
					{
						Log.e(TAG, "Encountered line too long :<");
						fail = true;
						go = false;
					}
					
				}
			}
		} catch (IOException e1) {
			Log.e(TAG, "Failed to read char from stream");
			fail = true;
		}
		if(fail != true)
		{
			int index = 0;
			int offt = 0;
			float[] vertData = new float[numOfVerticies * 5];
			for(int i = 0; i < faces.size(); i += 2)
			{
				offt = (faces.get(i) - 1) * 3;
				vertData[index++] = verts.get(offt);
				vertData[index++] = verts.get(offt + 1);
				vertData[index++] = verts.get(offt + 2);
			}
			for(int i = 1; i < faces.size(); i += 2)
			{
				offt = (faces.get(i) - 1) * 2;
				vertData[index++] = texCoords.get(offt);
				vertData[index++] = texCoords.get(offt + 1);
			}
			verts.clear();
			texCoords.clear();
			faces.clear();
			
			result = new Mesh(true, numOfVerticies, true);
			result.setVerts(vertData);
		}
		try {
			is.close();
		} catch (IOException e) {
			// ignore
		}
		return result;
	}
	
	private void processLine()
	{
		int added = 0;
		String line;
		String[] floats;
		if((buff[0] == '\0') || (buff[0] == '#') || (buff[0] == ' '))
		{
			return;
		}
		
		if(buff[0] == 'v')
		{
			if(buff[1] == ' ') // it's a vertex
			{
				line = String.copyValueOf(buff, 3, bufPos - 3);
				floats = line.split(" ");
				if(floats.length != 3)
				{
					Log.e(TAG, "Encountered malformed input");
					Arrays.fill(buff, '\0');
					return;
				}
				try {
					verts.add(Float.valueOf(floats[0])); added++;
					verts.add(Float.valueOf(floats[1])); added++;
					verts.add(Float.valueOf(floats[2].trim()));
				} catch (NumberFormatException ex) {
					for(int i = 0; i < added; i++)
					{
						verts.remove(verts.size() - 1);
					}
					Log.e(TAG, "Encountered malformed input");
					Arrays.fill(buff, '\0');
					return;
				}
			}
			else if(buff[1] == 't') // it's a texture coordinate
			{
				line = String.copyValueOf(buff, 3, bufPos - 3);
				floats = line.split(" ");
				if(floats.length != 3)
				{
					Log.e(TAG, "Encountered malformed input");
					Arrays.fill(buff, '\0');
					return;
				}
				try {
					texCoords.add(Float.valueOf(floats[0])); added++;
					texCoords.add(Float.valueOf(floats[1])); added++;
				} catch (NumberFormatException ex) {
					for(int i = 0; i < added; i++)
					{
						texCoords.remove(verts.size() - 1);
					}
					Log.e(TAG, "Encountered malformed input");
					Arrays.fill(buff, '\0');
					return;
				}
			}
		}
		else if(buff[0] == 'f') // it's a face
		{
			line = String.copyValueOf(buff, 2, bufPos - 2);
			floats = line.split(" "); // ok so in this case they're INTS not FLOATS. pardon the confusion
			if(floats.length != 3)
			{
				Log.e(TAG, "Encountered malformed input");
				Arrays.fill(buff, '\0');
				return;
			}
			try {
				for(int i = 0; i < 3; i++)
				{
					String[] ints = floats[i].split("/");
					if(ints.length < 2)
					{
						Log.e(TAG, "Encountered malformed input");
						Arrays.fill(buff, '\0');
						return;
					}
					faces.add(Integer.valueOf(ints[0])); added++;
					faces.add(Integer.valueOf(ints[1])); added++;
				}
				numOfVerticies += 3; // only if it works
			} catch (NumberFormatException ex) {
				for(int i = 0; i < added; i++)
				{
					texCoords.remove(verts.size() - 1);
				}
				Log.e(TAG, "Encountered malformed input");
				Arrays.fill(buff, '\0');
				return;
			}
		}
		Arrays.fill(buff, '\0');
	}
}
