package edu.stevens.arpac.graphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import android.opengl.GLES20;
import android.util.Log;

/**
 * Represents a single mesh. Optionally indexed. May either specify RGBA colors directly or use 2D texture coordinates
 * 
 * @author Jeffrey Cochran
 *
 */
public class Mesh 
{
	private static final String TAG = "Mesh";
	private static final int UNBIND = 0;
	private static final int VERT_BUFFER = 0;
	private static final int INDEX_BUFFER = 1;
	private static final int SHORT_SIZE_BYTES = 2;
	private static final int FLOAT_SIZE_BYTES = 4;
	private IntBuffer buffers = IntBuffer.allocate(2);
	private FloatBuffer vertBufferData;
	private ShortBuffer indexBufferData = null;
	private int numOfVerts;
	private boolean isStatic;
	private boolean isTextured;
	
	/**
	 * Instantiates a new mesh
	 * @param isStatic Will the verticies of this mesh stay constant for a long period of time?
	 * @param numOfVerts How many actual verticies will be specified?
	 * @param isTextured Will this mesh specify texture coordinates?
	 */
	public Mesh(boolean isStatic, int numOfVerts, boolean isTextured)
	{
		this.isStatic = isStatic;
		this.numOfVerts = numOfVerts;
		GLES20.glGenBuffers(2, buffers);
		this.isTextured = isTextured;
	}
	
	/**
	 * Set the vertex array object. Should be of the form
	 * <br><br>
	 * v1x, v1y, v1z, v2x, v2y, ..., vnz, t1u, t1v, t2u, t2v, ..., tnv
	 * <br><br>
	 * where v is a vertex and t is a texture coordinate or
	 * <br><br>
	 * v1x, v1y, v1z, v2x, v2y, ..., vnz, c1r, c1g, c1b, c1a, ..., cna
	 * <br><br>
	 * where v is a vertex and c is a color. After the call to this method returns it is safe to delete the float array containing the VAO
	 * @param verts The VAO
	 */
	public void setVerts(float[] verts)
	{
		if(verts.length < (3 * numOfVerts))
		{
			Log.e(TAG, "setVerts called with small array!");
			return;
		}
		vertBufferData = ByteBuffer.allocateDirect(verts.length
                * FLOAT_SIZE_BYTES).order(ByteOrder.nativeOrder()).asFloatBuffer();
		vertBufferData.put(verts).position(0);
		vertBufferData.limit(verts.length);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers.get(VERT_BUFFER));
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, FLOAT_SIZE_BYTES * vertBufferData.capacity(), vertBufferData, (isStatic) ? GLES20.GL_STATIC_DRAW : GLES20.GL_STREAM_DRAW);
		checkGlError("glBufferData");
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, UNBIND);
	}
	
	/**
	 * Set the index buffer, if one is to be used. If you were using an index buffer and would no longer like to use one (i.e., go back to array drawing), pass in null as the parameter
	 * @param indicies The index buffer of shorts, or null
	 */
	public void setIndexBuffer(short[] indicies)
	{
		if(indicies == null)
		{
			indexBufferData = null;
			return;
		}
		indexBufferData = ByteBuffer.allocateDirect(indicies.length
                * SHORT_SIZE_BYTES).order(ByteOrder.nativeOrder()).asShortBuffer(); 
		indexBufferData.put(indicies).position(0);
		indexBufferData.limit(indicies.length);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers.get(INDEX_BUFFER));
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, SHORT_SIZE_BYTES * indexBufferData.capacity(), indexBufferData, (isStatic) ? GLES20.GL_STATIC_DRAW : GLES20.GL_STREAM_DRAW);
		checkGlError("glBufferData");
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, UNBIND);
	}
	
	/**
	 * Renders this mesh. Infers appropriate rendering method from parameters and data structures.  
	 * @param mode The rendering mode to be used 
	 * @param posIndex The handle of the position attribute in the bound shader program 
	 * @param colorIndex The handle of the texture coordinate attribute or color attribute in the bound shader program. Whichever is appropriate wrt the VAO
	 */
	public void render(int mode, int posIndex, int colorIndex)
	{
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, buffers.get(VERT_BUFFER));
		checkGlError("glBindBuffer");
		GLES20.glEnableVertexAttribArray(posIndex);
		checkGlError("glEnableVertextAttribArray");
		GLES20.glEnableVertexAttribArray(colorIndex);
		checkGlError("glEnableVertexAttribArray");
		GLES20.glVertexAttribPointer(posIndex, 3, GLES20.GL_FLOAT, false, 0, 0);
		checkGlError("glVertexAttribPointer:position array");
		if(!isTextured)
		{
			GLES20.glVertexAttribPointer(colorIndex, 4, GLES20.GL_FLOAT, false, 0, FLOAT_SIZE_BYTES * 3 * numOfVerts);
			checkGlError("glVertexAttribPointer:colorArray");
		}
		else
		{
			GLES20.glVertexAttribPointer(colorIndex, 2, GLES20.GL_FLOAT, false, 0, FLOAT_SIZE_BYTES * 3 * numOfVerts);
			checkGlError("glVertexAttribPointer:textured");
		}
		
		if(indexBufferData != null)
		{
			GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, buffers.get(INDEX_BUFFER));
			GLES20.glDrawElements(mode, indexBufferData.capacity(), GLES20.GL_UNSIGNED_SHORT, 0);
			checkGlError("glDrawElements");
		}
		else
		{
			GLES20.glDrawArrays(mode, 0, numOfVerts);
			checkGlError("glDrawArrays");
		}
		
		
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, UNBIND);
		
		if(indexBufferData != null)
		{
			GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, UNBIND);
		}
	}
	
	private void checkGlError(String op) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, op + ": glError " + error);
            throw new RuntimeException(op + ": glError " + error);
        }
    }
}
