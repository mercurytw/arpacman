package edu.stevens.arpac.graphics;

import java.nio.IntBuffer;
import java.util.HashMap;

import android.opengl.GLES20;
import android.util.Log;

/**
 * Represents a compiled vertex and fragment GLSL shader.
 * @author Jeff Cochran
 *
 */
public class ShaderProgram 
{
	private static final String TAG = "ShaderProgram";
	
	/**
	 * Default vertex shader. Handles vec4 colors or vec2 texture coordinates. Switches between the two
	 * behaviors by checking the u_isTextured uniform.
	 */
	public static final String DEFAULT_VERT_SHADER = "attribute vec4 a_position;\n" +
													 "attribute vec4 a_color;\n" +
													 "attribute vec2 a_texCoord;\n" +
													 "uniform float u_isTextured;\n" +
													 "uniform mat4 u_worldView;\n" +
													 "varying vec4 v_color; \n" +
													 "varying float v_textured;\n" +
													 "varying vec2 v_texCoord;\n" +
													 "void main(){\n" +
													 "v_textured = u_isTextured;\n" +
													 "if(u_isTextured == 1.0){\n" +
													 "v_texCoord = a_texCoord;\n" +
													 "}else{\n" +
													 "v_color = vec4(1,1,1,1) * a_color;}\n" +
													 "gl_Position = u_worldView * a_position;\n" +
													 "}";
	
	/**
	 * Default fragment shader. Handles vec4 colors or vec2 texture coordinates. Switches between the two
	 * behaviors by checking the v_textured varying.
	 */
	public static final String DEFAULT_FRAG_SHADER = "#ifdef GL_ES\n" +
													 "precision mediump float;\n" +
													 "#endif\n" +
													 "varying vec2 v_texCoord;\n" +
													 "varying vec4 v_color;\n" +
													 "varying float v_textured;\n" +
													 "uniform sampler2D u_texture;\n" +
													 "void main(){\n" +
													 "if(v_textured != 0.0){\n" +
													 "gl_FragColor = texture2D(u_texture, v_texCoord);\n" +
													 "}else{\n" +
													 "gl_FragColor = v_color;}\n" +
													 "}";
	
	private int shaderName;
	private int vertexShader;
	private int fragmentShader;
	private String[] attributeNames;
	private String[] uniformNames;
	private HashMap<String, Integer> uniformLookupTable;
	private HashMap<String, Integer> attributeLookupTable;
	private static ShaderProgram defaultShader = null;
	
	//http://stackoverflow.com/a/4857931
	/**
	 * Creates a shader program by compiling the vertex and fragment source strings. These may have been specified as hardcoded strings or loaded from files.
	 * @param vertexSource The source of the vertex shader
	 * @param fragmentSource The source of the fragment shader
	 */
	public ShaderProgram(String vertexSource, String fragmentSource)
	{
		vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexSource);
	    fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentSource);
	    
	    attributeLookupTable = new HashMap<String, Integer>();
	    uniformLookupTable = new HashMap<String, Integer>();

	    shaderName = GLES20.glCreateProgram();
	    if (shaderName != 0) {
	        GLES20.glAttachShader(shaderName, vertexShader);
	        checkGlError("glAttachShader");
	        GLES20.glAttachShader(shaderName, fragmentShader);
	        checkGlError("glAttachShader");
	        GLES20.glLinkProgram(shaderName);
	        int[] linkStatus = new int[1];
	        GLES20.glGetProgramiv(shaderName, GLES20.GL_LINK_STATUS, linkStatus, 0);
	        if (linkStatus[0] != GLES20.GL_TRUE) {
	            Log.e(TAG, "Could not link program: ");
	            Log.e(TAG, GLES20.glGetProgramInfoLog(shaderName));
	            GLES20.glDeleteProgram(shaderName);
	            shaderName = 0;
	        }
	    }
	    if(shaderName != 0)
	    {
	    	setupAttributes();
	    	setupUniforms();
	    }
	}
	
	private int loadShader(int shaderType, String source) {
	    int shader = GLES20.glCreateShader(shaderType);
	        if (shader != 0) {
	            GLES20.glShaderSource(shader, source);
	            GLES20.glCompileShader(shader);
	            int[] compiled = new int[1];
	            GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
	            if (compiled[0] == 0) {
	                Log.e(TAG, "Could not compile shader " + shaderType + ":");
	                Log.e(TAG, GLES20.glGetShaderInfoLog(shader));
	                GLES20.glDeleteShader(shader);
	                shader = 0;
	            }
	        }
	        return shader;
	}
	
	private void checkGlError(String op) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, op + ": glError " + error);
            throw new RuntimeException(op + ": glError " + error);
        }
    }
	
	private IntBuffer attribs = IntBuffer.allocate(1);
	private int[] attribsData = new int[3];
	private void setupAttributes()
	{
		int numAttribs;
		GLES20.glGetProgramiv(shaderName, GLES20.GL_ACTIVE_ATTRIBUTES, attribs);
		numAttribs = attribs.get(0);
		
		attributeNames = new String[numAttribs];
		byte[] name = new byte[256];
		attribsData[0] = 256;
		attribsData[1] = 0;
		attribsData[2] = 0;
		int value;
		for(int i = 0; i < attributeNames.length; i++)
		{
			// holy fuck almighty this function...
			GLES20.glGetActiveAttrib(shaderName, i, 256, attribsData, 0, attribsData, 1, attribsData, 2, name, 0);
			char[] nameChars = new char[attribsData[0]];
			for(int j = 0; j < attribsData[0]; j++)
			{
				nameChars[j] = (char)name[j];
			}
			attributeNames[i] = new String(nameChars);
			value = GLES20.glGetAttribLocation(shaderName, attributeNames[i]);
			attributeLookupTable.put(attributeNames[i], value);
		}
	}
	
	private void setupUniforms()
	{
		int numAttribs;
		GLES20.glGetProgramiv(shaderName, GLES20.GL_ACTIVE_UNIFORMS, attribs);
		numAttribs = attribs.get(0);
		
		uniformNames = new String[numAttribs];
		byte[] name = new byte[256];
		attribsData[0] = 256;
		attribsData[1] = 0;
		attribsData[2] = 0;
		int value;
		for(int i = 0; i < uniformNames.length; i++)
		{
			GLES20.glGetActiveUniform(shaderName, i, 256, attribsData, 0, attribsData, 1, attribsData, 2, name, 0);
			char[] nameChars = new char[attribsData[0]];
			for(int j = 0; j < attribsData[0]; j++)
			{
				nameChars[j] = (char)name[j];
			}
			uniformNames[i] = new String(nameChars);
			value = GLES20.glGetUniformLocation(shaderName, uniformNames[i]);
			uniformLookupTable.put(uniformNames[i], value);
		}
	}
	
	/**
	 * Gets the handle of a known attribute
	 * @param name The name of the attribute in the shader source
	 * @return the attribute handle
	 */
	public int getAttribLocation(String name)
	{
		Integer result = -1;
		if((result = attributeLookupTable.get(name)) == null)
		{
			Log.e(TAG, "Couldn't find attribute " + name);
			return -1;
		}
		return result;
	}
	
	/**
	 * Gets the handle of a known uniform
	 * @param name The name of the uniform in the shader source
	 * @return the uniform handle
	 */
	public int getUniformLocation(String name)
	{
		Integer result = -1;
		if((result = uniformLookupTable.get(name)) == null)
		{
			Log.e(TAG, "Couldn't find uniform " + name);
			return -1;
		}
		return result;
	}
	
	/**
	 * Wraps glUseProgram
	 */
	public void use()
	{
		GLES20.glUseProgram(shaderName);
		checkGlError("glUseProgram");
	}
	
	/**
	 * Construct a new ShaderProgram using the default vertex and fragment shaders
	 * @return The default shader program
	 */
	public static ShaderProgram getDefaultShader()
	{
		if(defaultShader == null)
		{
			defaultShader = new ShaderProgram(DEFAULT_VERT_SHADER, DEFAULT_FRAG_SHADER);
		}
		return(defaultShader);
	}
}
