package edu.stevens.arpac.graphics;

import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;

/**
 * A class representing a 2D OpenGL texture
 * @author Jeff Cochran
 *
 */
public class Texture 
{
	private static final String TAG = "Texture";
	private static final int UNBIND = 0;
	private int[] textureHandle;
	
	/**
	 * Instantiate a new 2D OpenGL texture from the source bitmap. The width and height of the bitmap should be powers of two. 
	 * Make sure the images are placed in the assets folder, otherwise scaling may occur when the android library loads them.
	 * <br> Uses GL_LINEAR for min and mag filter and does S&T edge clamping
	 * @param sourceData The bitmap to use as source data. Width and height should be powers of two
	 */
	public Texture(Bitmap sourceData)
	{
		textureHandle = new int[1];
		GLES20.glGenTextures(1, textureHandle, 0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);
		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, sourceData, 0);
		checkGlError("glTexImage2d");
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, UNBIND);
	}
	
	/**
	 * Bind this texture to the given texture unit
	 * @param textureUnitNumber
	 */
	public void bindToUnit(int textureUnitNumber)
	{
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + textureUnitNumber);
	}
	
	/**
	 * Unbind this texture from the texture unit
	 */
	public void unbind()
	{
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, UNBIND);
	}
	
	private void checkGlError(String op) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, op + ": glError " + error);
            throw new RuntimeException(op + ": glError " + error);
        }
    }
}
