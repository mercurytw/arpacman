package edu.stevens.arpac.graphics;

import edu.stevens.arpac.math.Quaternion;
import android.opengl.Matrix;

/**
 * A class representing a 3D graphics "camera" or "eye." Contains and maintains all of the following:
 * <ul>
 * <li> The projection matrix
 * <li> The View matrix
 * <li> the look vector
 * <li> the up vector
 * <li> the camera position & orientation
 * </ul>
 * @author Jeff Cochran
 *
 */
public class GameCamera 
{
	@SuppressWarnings("unused")
	private static final String TAG = "GameCamera";
	private static final float DEG_TO_RAD = (float) (Math.PI / 180.0f);
	
	private float[] projection; 
	private float[] view;
	private float[] combined;
	
	private float[] eye;
	private float[] at;
	private float[] up;
	
	private float[] scratchpad = new float[32];
	
	private float near, far;
	
	private Quaternion rotation;
	
	private boolean cameraIsDirty = false;
	private boolean combinedIsStale = true;
	
	/**
	 * Instantiate a new GameCamera and automatically calculate an appropriate projection matrix
	 * given the width and height of the android screen in pixels.
	 * @param width The width of the screen in pixels
	 * @param height The height of the screen in pixels
	 */
	public GameCamera(float width, float height)
	{
		near = 2.0f;
		far = 200.0f;
		projection = new float[16];
		float ratio = (float) width / height;
        Matrix.frustumM(projection, 0, -ratio, ratio, -1, 1, near, far);
        
        view = new float[16];
        Matrix.setLookAtM(view, 0, 
		          0.0f, 0.0f, 1.0f, 
		          0.0f, 0.0f, -1.0f, 
		          0.0f, 1.0f, 0.0f);
        
        eye = new float[] { 0.0f, 0.0f, 1.0f, 1.0f };
        at = new float[] { 0.0f, 0.0f, -1.0f, 1.0f };
        up = new float[] { 0.0f, 1.0f, 0.0f, 1.0f };
         
        combined = new float[16];
        
        rotation = new Quaternion(Quaternion.IDENTITY);
	}
	
	/**
	 * Reset the projection matrix to an appropriate matrix given the width and height of the 
	 * android screen in pixels
	 * @param width The width of the screen in pixels
	 * @param height The height of the screen in pixels
	 */
	public void setAspect(float width, float height)
	{
		float ratio = (float) width / height;
        Matrix.frustumM(projection, 0, -ratio, ratio, -1, 1, near, far);
	}
	
	/**
	 * Get the projection matrix
	 * @return A 4x4 float matrix 
	 */
	public float[] getProjectionMat()
	{
		return projection;
	}
	
	/**
	 * Get the view matrix
	 * @return A 4x4 float matrix 
	 */
	public float[] getViewMat()
	{
		updateView();
		return view;
	}
	
	/**
	 * Get the view matrix and store it in the provided matrix
	 * @param mat a 4x4 column-major float matrix
	 * @param offt the offset in mat to store the data
	 */
	public void getViewMat(float[] mat, int offt)
	{
		updateView();
		for(int i = 0; i < 16; i++)
		{
			mat[i+offt] = view[i];
		}
	}
	
	private boolean manual = false;
	public void setViewDirect(float[] mat, int offt, float x, float y, float z)
	{
		manual = true;
		view[0] = mat[0 + offt];
		view[1] = mat[1 + offt];
		view[2] = mat[2 + offt];
		
		view[4] = mat[3 + offt];
		view[5] = mat[4 + offt];
		view[6] = mat[5 + offt];
		
		view[8] = mat[6 + offt];
		view[9] = mat[7 + offt];
		view[10] = mat[8 + offt];
		
		view[12] = x;
		view[13] = y;
		view[14] = z;
		cameraIsDirty = false;
		combinedIsStale = true;
	}
	
	private int counter = 0;
	/**
	 * Rotate the camera about an arbitrary axis
	 * @param theta Angle of rotation in degrees
	 * @param x
	 * @param y
	 * @param z
	 */
	public void rotate(float theta, float x, float y, float z)
	{
		rotation.rotate(theta * DEG_TO_RAD, x, y, z);
		if(counter++ >= 10)
		{
			rotation.normalize();
			counter = 0;
		}
		cameraIsDirty = true;
		combinedIsStale = true;
	}
	
	/**
	 * Set the camera's orientation
	 * @param orient The new orientation of the camera
	 */
	public void setOrientation(Quaternion orient)
	{
		rotation.copy(orient);
		cameraIsDirty = true;
		combinedIsStale = true;
	}
	
	private void updateView()
	{
		if((!cameraIsDirty) || (manual))
		{
			return;
		}
		at[0] -= eye[0];
		at[1] -= eye[1];
		at[2] -= eye[2];
		rotation.rotate(scratchpad, 0, at, 0);
		rotation.rotate(scratchpad, 4, up, 0);
		scratchpad[0] += eye[0];
		scratchpad[1] += eye[1];
		scratchpad[2] += eye[2];
		at[0] += eye[0];
		at[1] += eye[1];
		at[2] += eye[2];
		Matrix.setLookAtM(view, 0,
						  eye[0], eye[1], eye[2],
						  scratchpad[0], scratchpad[1], scratchpad[2],
						  scratchpad[4], scratchpad[5], scratchpad[6]);
		cameraIsDirty = false;
	}
	
	/**
	 * Set the position of the camera
	 * @param x
	 * @param y
	 * @param z
	 */
	public void setPosition(float x, float y, float z)
	{
		float dx, dy, dz;
		dx = x - eye[0];
		dy = y - eye[1];
		dz = z - eye[2];
		translate(dx, dy, dz);
	}
	
	/**
	 * Move the camera in the direction specified by [x,y,z] with respect to world space
	 * @param x
	 * @param y
	 * @param z
	 */
	public void translate(float x, float y, float z)
	{
		Matrix.setIdentityM(scratchpad, 0);
		Matrix.translateM(scratchpad, 0, x, y, z);
		Matrix.multiplyMV(eye, 0, scratchpad, 0, eye, 0);
		Matrix.multiplyMV(at, 0, scratchpad, 0, at, 0);
		cameraIsDirty = true;
		combinedIsStale = true;
	}
	
	private float[] tmpVec = new float[4];
	/**
	 * Move the camera in the direction specified by [x, y, z] with respect to camera space 
	 * @param x
	 * @param y
	 * @param z
	 */
	public void move(float x, float y, float z)
	{
		tmpVec[0] = x;
		tmpVec[1] = y;
		tmpVec[2] = z;
		tmpVec[3] = 1.0f;
		rotation.rotate(tmpVec, tmpVec);
		translate(tmpVec[0], tmpVec[1], tmpVec[2]);
	}
	
	/**
	 * Get the combined projection and view matrix
	 * @return A 4x4 float matrix
	 */
	public float[] getCombined()
	{
		updateView();
		if(combinedIsStale)
		{
			Matrix.multiplyMM(combined, 0, projection, 0, view, 0);
			combinedIsStale = false;
		}
		return combined;
	}
	
	/**
	 * Return the current position of the camera in world space
	 * @return a float-4 representing the camera position
	 */
	public float[] getPosition()
	{
		return eye;
	}
	
	/**
	 * Return the current orientation of the camera in world space
	 * @return A Quaternion representing camera orientation
	 */
	public Quaternion getOrientation()
	{
		return rotation;
	}
}
