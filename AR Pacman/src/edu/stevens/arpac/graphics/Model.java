package edu.stevens.arpac.graphics;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;
import edu.stevens.arpac.math.Quaternion;

/**
 * container for a mesh, shader and optional texture. all variables are protected so that if special render logic needs to be 
 * applied, then this class can be anonymously subclassed at instantiation time. Please read the {@link Model#render} method if you plan
 * on instantiating this class. This class makes assumptions about variable names in the shader program which must hold true for this class
 * to render properly.
 * @author Jeff Cochran
 *
 */
public class Model extends Object
{
	private static final String TAG = "Model";
	private static final float DEG_TO_RAD = (float) (Math.PI / 180.0f);
	protected Quaternion orientation;
	protected Mesh mesh;
	protected Texture tex;
	protected float[] modelMat;
	protected ShaderProgram program;
	protected float[] mvpMat = new float[16]; // for use in the render function so you don't need to allocate a mat
	public String name;
	private static int instances = 0;
	
	/**
	 * Instantiate a new model. If the texture parameter is null then it is assumed that the mesh contains colors instead
	 * of texture coordinates in it's vertex data.
	 * @param m The mesh to use
	 * @param tex The texture to be used. May be null.
	 * @param shader the compiled shader program to use
	 */
	public Model(Mesh m, Texture tex, ShaderProgram shader)
	{
		name = "Model#" + instances++;
		program = shader;
		mesh = m;
		this.tex = tex;
		modelMat = new float[16];
		Matrix.setIdentityM(modelMat, 0);
		orientation = new Quaternion(Quaternion.IDENTITY);
	}
	
	/**
	 * Instantiate a new model. If the texture parameter is null then it is assumed that the mesh contains colors instead
	 * of texture coordinates in it's vertex data.
	 * @param name The name of the mesh
	 * @param m The mesh to use
	 * @param tex The texture to be used. May be null.
	 * @param shader the compiled shader program to use
	 */
	public Model(String name, Mesh m, Texture tex, ShaderProgram shader)
	{
		this.name = name + instances++; 
		program = shader;
		mesh = m;
		this.tex = tex;
		modelMat = new float[16];
		Matrix.setIdentityM(modelMat, 0);
		orientation = new Quaternion(Quaternion.IDENTITY);
	}
	
	private int counter = 0;
	/**
	 * 
	 * @param angle in degrees
	 * @param x
	 * @param y
	 * @param z
	 */
	public void rotate(float angle, float x, float y, float z)
	{
		orientation.rotate(angle * DEG_TO_RAD, x, y, z);
		if(counter++ == 5)
		{
			counter = 0;
			orientation.normalize();
		}
	}
	
	/**
	 * 
	 * @param angle in degrees
	 * @param x
	 * @param y
	 * @param z
	 */
	public void setRotation(float angle, float x, float y, float z)
	{
		orientation.set(angle, x, y, z);
	}
	
	/**
	 * Translate the model by the vector [x, y, z] with respect to world coordinates
	 * @param x
	 * @param y
	 * @param z
	 */
	public void translate(float x, float y, float z)
	{
		Matrix.translateM(modelMat, 0, x, y, z);
	}
	
	/**
	 * Scale the model by the given scale factors
	 * @param sx float x scale factor
	 * @param sy float y scale factor
	 * @param sz float z scale factor
	 */
	public void scale( float sx, float sy, float sz )
	{
		Matrix.scaleM(modelMat, 0, sx, sy, sz);
	}
	
	/**
	 * Set the position of this model in world space
	 * @param x
	 * @param y
	 * @param z
	 */
	public void setPosition(float x, float y, float z)
	{
		modelMat[11] = 0.0f;
		modelMat[12] = 0.0f;
		modelMat[13] = 0.0f;
		Matrix.translateM(modelMat, 0, x, y, z);
	}
	
	/**
	 * Get the current orientation of this model in world space
	 * @return A quaternion representing this model's orientation
	 */
	public Quaternion getOrientation()
	{
		return orientation;
	}
	
	protected void createMVP(float[] vpMat)
	{
		orientation.toRotMat(mvpMat, false, 0);
		Matrix.multiplyMM(mvpMat, 0, modelMat, 0, mvpMat, 0);
		Matrix.multiplyMM(mvpMat, 0, vpMat, 0, mvpMat, 0);
	}
	
	/**
	 * This may be redefined in a subclass for specially rendered meshes.
	 * @param vpMat ViewProjection combined matrix from the camera
	 */
	public void render(float[] vpMat)
	{
		program.use();
		
		int pos, color, worldView, textured;
		pos = program.getAttribLocation("a_position");
		checkGlError("getAttribLocation");
		worldView = program.getUniformLocation("u_worldView");
		checkGlError("getUniformLocation");
		textured = program.getUniformLocation("u_isTextured");
		checkGlError("getUniformLocation");
		if(tex == null)
		{
			color = program.getAttribLocation("a_color");
			GLES20.glUniform1f(textured, 0.0f);
			checkGlError("glUniform1f");
		}
		else
		{
			color = program.getAttribLocation("a_texCoord");
			GLES20.glUniform1f(textured, 1.0f);
			checkGlError("glUniform1f");
		}
		createMVP(vpMat);
		
		GLES20.glUniformMatrix4fv(worldView, 1, false, mvpMat, 0);
		checkGlError("glUniofrmMatrix4fv");
		if(tex != null){ tex.bindToUnit(0); checkGlError("tex.bindToUnit"); }
		mesh.render(GLES20.GL_TRIANGLES, pos, color);
		if(tex != null){ tex.unbind(); }
		GLES20.glUseProgram(0);
	}
	
	private void checkGlError(String op) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, op + ": glError " + error);
            throw new RuntimeException(op + ": glError " + error);
        }
    }
	
	@Override
	public String toString()
	{
		return name;
	}
}
