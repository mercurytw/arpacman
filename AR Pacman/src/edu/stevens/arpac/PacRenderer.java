package edu.stevens.arpac;

import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import edu.stevens.arpac.engine.Actor;
import edu.stevens.arpac.engine.ITickable;
import edu.stevens.arpac.engine.ResourceManager;
import edu.stevens.arpac.graphics.GameCamera;
import edu.stevens.arpac.math.Quaternion;
import edu.stevens.arpac.webclient.PtammLocationProvider;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class PacRenderer implements Renderer, OnTouchListener
{
	private static final String TAG = "PacRenderer";
	private GameCamera camera = null;
	private ResourceManager resources;
	private boolean gameOver = false;
	private ArrayList<Actor> actors = new ArrayList<Actor>();
	private ArrayList<ITickable> tickList = new ArrayList<ITickable>();
	
	private CVVideoLocationProvider provider = null;
	
	private float[] scratchpad = new float[64];
	private PlayerCamera playerCamCtl = null;
	private Level scenario;
	public float aspectRatio = 1.0f;
	public float worldScale = 1.0f;
	
	public PacRenderer(Context ctx, CVVideoLocationProvider provider)
	{
		resources = new ResourceManager(ctx);
		resources.addBitmap("shrug", "shrug.bmp");
		resources.addBitmap("science", "scienceDog.bmp");
		this.provider = provider;
		if(provider == null)
		{
			Log.d(TAG, "Provider is null, falling back to manual controls");
		}
	}
	
	private float distSq(float x1, float y1, float x2, float y2)
	{
		return( ((x2 -x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
	}
	
	private float[] lastPlayerPos = null;
	private float[] velocity = null;
	private boolean shouldDoInit = false;
	private long timeOfLastFrameMillis = 0l;
	private boolean gameStart = false;
	private long posDumpAgg = 0;
	public void onDrawFrame(GL10 unused) 
	{
		long timeNow = System.currentTimeMillis();
		if(timeOfLastFrameMillis == 0l)
		{
			Log.d(TAG, "===============================");
			Log.d(TAG, "Ready");
			Log.d(TAG, "===============================");
			timeOfLastFrameMillis = timeNow;
			
			if(provider == null)
			{
				gameStart = true;
				initWorld();
			}
		}
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		GLES20.glClearDepthf(100.0f);
		GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
		
		if(shouldDoInit)
		{
			shouldDoInit = false;
			initWorld();
		}
		
		if(gameStart)
		{
			float[] cpos = camera.getPosition();

			if(!gameOver)
			{
				if(provider == null)
				{
					playerCamCtl.moveCamera(timeNow - timeOfLastFrameMillis);
				}
				else
				{
					int quality = provider.locProv.getCameraPose(scratchpad, 0, scratchpad, 4);
					scratchpad[3] = 1.0f;
					Quaternion tmp = provider.getQuatFromLie(scratchpad, 4);
					if((quality != -1) && (tmp != null))
					{
						if((quality == 0) && (velocity != null))
						{
							lastPlayerPos[0] += velocity[0];
							lastPlayerPos[1] += velocity[1];
							lastPlayerPos[2] += velocity[2];
							// XXX: Uncomment this
							//camera.setPosition(lastPlayerPos[0], lastPlayerPos[1], lastPlayerPos[2]);
						}
						else
						{
							provider.transformPose(scratchpad, 7, scratchpad, tmp);
							if(lastPlayerPos == null)
							{
								lastPlayerPos = new float[3];
							}
							else
							{
								if(velocity == null)
								{
									velocity = new float[3];
								}
								velocity[0] = scratchpad[7] - lastPlayerPos[0];
								velocity[1] = scratchpad[8] - lastPlayerPos[1];
								velocity[2] = scratchpad[9] - lastPlayerPos[2];
							}
							lastPlayerPos[0] = scratchpad[7];
							lastPlayerPos[1] = scratchpad[8];
							lastPlayerPos[2] = scratchpad[9];
							camera.setPosition(scratchpad[7], 0.0f, scratchpad[9]);
							//camera.setOrientation(tmp);
							if( (posDumpAgg += (timeNow - timeOfLastFrameMillis)) >= 5000l )
							{
								posDumpAgg = 0l;
								float deleteThis[] = camera.getPosition();
								Log.d(TAG, "Reported from PTAMM:{" + scratchpad[0] + ", " + scratchpad[1] + ", " + scratchpad[2] + "}");
								Log.d(TAG, "current camera pos: {" + deleteThis[0] + ", " + deleteThis[1] + ", " + deleteThis[2] + "}");
							}
						}
					}
				}
				
				for(int i = 0; i < tickList.size(); i++)
				{
					ITickable t = tickList.get(i);
					((GhostController) t).setTarget(cpos[0], cpos[2]);
					//XXX: uncomment here for ghosts to chase camera
					//t.tick(timeNow - timeOfLastFrameMillis);
				}
			}

			for(int i = 0; i < actors.size(); i++)
			{
				Actor act = actors.get(i);
				if(act.checkActive())
				{
					float radsq = (act.radius) * (act.radius);
					if(radsq >= distSq(act.pos[0], act.pos[1], cpos[0], cpos[2]))
					{
						if(act.reactor != null)
						{
							act.reactor.handleCollision();
						}
						else
						{
							act.setActive(false);
							Log.d(TAG, "waka");
						}
					}
					else
					{
						act.draw(camera.getCombined());
					}
				}
			}
		}
		
		timeOfLastFrameMillis = timeNow;
	}

	public void onSurfaceChanged(GL10 unused, int width, int height) 
	{
		GLES20.glViewport(0, 0, width, height);
		
		if(camera == null)
		{
			camera = new GameCamera(width * 1.0f, height * 1.0f);
			camera.setPosition(0.0f, 0.0f, 2.0f);
			playerCamCtl = new PlayerCamera(camera, width, height);
		}
		else
		{
			camera.setAspect(width * 1.0f, height * 1.0f);
		}
	}
	
	private void initWorld()
	{	
		scenario.setup( aspectRatio, worldScale );
		
		Log.d(TAG, "===============================");
		Log.d(TAG, "Game Initialized");
		Log.d(TAG, "===============================");
	}

	public void onSurfaceCreated(GL10 unused, EGLConfig config) 
	{
		GLES20.glEnable(GLES20.GL_CULL_FACE);
		GLES20.glCullFace(GLES20.GL_BACK);
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);
		GLES20.glDepthMask(true);
		checkGlError("Failed to set initial GLES2.0 options");
		
		resources.addMesh("cube", "testcube.obj");
		resources.addTexture("shrug", "shrug.bmp");
		resources.addTexture("science", "scienceDog.bmp");
		resources.addTexture("pc", "porkchops.jpg");
		
		scenario = new Level("level_layout.json", resources, actors, tickList, new Actor.CollisionHandler() {
			public void handleCollision() {
				gameOver = true;
			}
		});
	}
	
	private void checkGlError(String op) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, op + ": glError " + error);
            throw new RuntimeException(op + ": glError " + error);
        }
    }
	
	private boolean ptammInit(int action)
	{
		if((action == MotionEvent.ACTION_DOWN) || 
		   (action == MotionEvent.ACTION_POINTER_DOWN))
		{
			if(provider != null)
			{
				if(!provider.locProv.isPtammInitialized())
				{
					if(provider.locProv.getState() == PtammLocationProvider.PtammState.READY)
					{
						provider.locProv.captureInitFrame();
					}
					return false;
				}
				return provider.calculateBasisChange(100.0f, 100.0f);
			}
	    }
		return true;
	}
	
	public boolean onTouch(View v, MotionEvent event) 
	{
		int action = event.getAction() & MotionEvent.ACTION_MASK;
		int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> 
		MotionEvent.ACTION_POINTER_INDEX_SHIFT;
		int pointerId = event.getPointerId(pointerIndex);
		float x = event.getX(pointerIndex);
		float y = event.getY(pointerIndex);
		
		if(ptammInit(action) == false)
		{
			return false;
		}
		else if(!gameStart)
		{
			shouldDoInit = true;
			gameStart = true;
			if( provider != null )
			{
				aspectRatio = provider.worldAspect;
				worldScale = provider.worldScale;
			} 
			return true;
		}
		playerCamCtl.handleTouch(action, pointerId, x, y);
		
		return true;
	}

}
