#pragma version(1)
#pragma rs java_package_name(edu.stevens.arpac.webclient)
#pragma rs_fp_relaxed

rs_allocation Y;
const int8_t* UV;
int32_t *optionalOut;
uint32_t *kTable;
rs_allocation out;

static int gWidth;
static int gHeight;
static int gSize;
static int g2Width;

rs_script script;

typedef struct pair_t {
	int32_t colorReverse;
	int32_t colorForward;
} pair;

static pair getRgb(int32_t y, int32_t u, int32_t v)
{
	pair result;
	int32_t r,g,b;
	
	r = y + (int32_t) (1.402f*v); // 1.402
	g = y - (int32_t) ((0.344f*u) + (0.714f*v));
	b = y + (int32_t) (1.772f*u); // 1.772
	
	r = (r > 255) ? 255 : ((r < 0) ? 0 : r);
	g = (g > 255) ? 255 : ((g < 0) ? 0 : g);
	b = (b > 255) ? 255 : ((b < 0) ? 0 : b);
	
	result.colorReverse = 0xff000000 | (r<<16) | (g<<8) | b;
	result.colorForward = (b<<24) | (g<<16) | (r<<8) | 0x000000ff;
	
	return(result);
}

void root(const int8_t* v_in, int32_t *v_out, 
		  const void* usrData, uint32_t x, uint32_t y)
{
	int32_t color;
	pair res;
	
	uint32_t k = kTable[x];
	
	int32_t y1 = (*v_in);
	int32_t u  = UV[k];
	int32_t v  = UV[k+1];
	y1 &= 0x000000ff;
	u &= 0x000000ff;
	v &= 0x000000ff;
	
	u = u - 128;
	v = v - 128;
	
	res = getRgb(y1,u,v);
	if(optionalOut != 0)
	{
		optionalOut[x] = res.colorForward;
	}
	
	(*v_out) = res.colorReverse;
}

void yuvToRgb()
{
	rsForEach(script, Y, out, 0);
}

void setSize(int width, int height)
{
	gWidth = width;
	g2Width = width * 2;
	gHeight = height;
	gSize = width * height;
	uint32_t k, l;
	for(uint32_t x = 0; x < gSize; x++)
	{
		k = ( x / g2Width ) * gWidth;
		l = ( x % g2Width ) % gWidth;
		k += ( l / 2 ) * 2;
		kTable[x] = k;
	}
}