package edu.stevens.arpac.webclient;

import java.util.concurrent.locks.ReentrantLock;

import android.util.Log;

/**
 * A pool of {@link ByteArrayElement}s. Thread-safe.
 * @author jcochran
 *
 */
public class ByteArrayElementPool 
{
	private static final String TAG = "ByteArrayElementPool";
	private int currentCapacity;
	private ByteArrayElement[] elements; 
	private ReentrantLock mutex;
	
	public ByteArrayElementPool( int initialCapacity )
	{
		currentCapacity = initialCapacity;
		elements = new ByteArrayElement[initialCapacity];
		
		for(int i = 0; i < initialCapacity; i++)
		{
			elements[i] = new ByteArrayElement( 0, 0, 0 );
		}
		
		mutex = new ReentrantLock();
	}
	
	// TO BE CALLED INSIDE CRITICAL SECTIONS ONLY
	private void grow()
	{
		int newCap = currentCapacity * 2;
		Log.i(TAG, "Growing pool to size " + newCap );
		
		ByteArrayElement[] newArr = new ByteArrayElement[newCap];
		for(int i = 0; i < currentCapacity; i++)
		{
			newArr[i] = elements[i];
		}
		for(int i = currentCapacity; i < newCap; i++)
		{
			newArr[i] = new ByteArrayElement( 0, 0, 0 );
		}
		elements = newArr;
		currentCapacity = newCap;
	}
	
	public ByteArrayElement get(long seqNo, long creationTime, long conversionTime)
	{
		try {
			mutex.lockInterruptibly();
		} catch (InterruptedException e) {
			Log.d(TAG, "get() interrupted while waiting for lock");
			return null;
		}
		ByteArrayElement res = null;
		for(int i = 0; i < currentCapacity; i++)
		{
			if( elements[i].isActive == false )
			{
				res = elements[i];
				break;
			}
		}
		if( res == null )
		{
			int oldcap = currentCapacity;
			grow();
			for(int i = oldcap; i < currentCapacity; i++)
			{
				if( elements[i].isActive == false )
				{
					res = elements[i];
					break;
				}
			}
			if( res == null )
			{
				Log.e(TAG, "Something went terribly wrong");
				return null;
			}
		}
		res.isActive = true;
		res.seqNum = seqNo;
		res.creationTime = creationTime;
		res.conversionTime = conversionTime;
		mutex.unlock();
		return(res);
	}
	
	public void reclaim(ByteArrayElement elet)
	{
		try {
			mutex.lockInterruptibly();
		} catch (InterruptedException e) {
			Log.d(TAG, "reclaim() interrupted while waiting for lock");
			return;
		}
		elet.isActive = false;
		elet.deactivate();
		mutex.unlock();
	}
}
