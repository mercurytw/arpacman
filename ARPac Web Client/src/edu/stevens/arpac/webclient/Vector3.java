package edu.stevens.arpac.webclient;

/**
 * A Vector3 of floats representing player position
 * @author Jeff Cochran
 *
 */
public class Vector3 
{
	public float x,y,z;
	
	/**
	 * Create a new Vector3 with values initialized to 0.0f
	 **/
	public Vector3()
	{
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;
	}
	
	/**
	 * Create a new Vector3 with values initialized to those provided to the ctor 
	 * @param x
	 * @param y
	 * @param z
	 */
	public Vector3(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Copy constructor 
	 * @param other Vector3 to be copied from
	 */
	public Vector3(Vector3 other)
	{
		this.x = other.x;
		this.y = other.y;
		this.z = other.z;
	}
	
	@Override
	public String toString()
	{
		return "{ " + x + ", " + y + ", " + z + " }";
	}
}
