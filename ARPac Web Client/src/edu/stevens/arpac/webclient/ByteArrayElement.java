package edu.stevens.arpac.webclient;

import java.io.ByteArrayOutputStream;

/**
 * A ByteArrayElement is a specialized {@link ByteArrayOutputStream} which is extended to
 *  implement {@link Comparable} for use in an ordered container. It also contains pooling data
 * @author jcochran
 *
 */
public class ByteArrayElement extends ByteArrayOutputStream implements Comparable<ByteArrayElement>
{
	public long seqNum;
	public long creationTime;
	public long conversionTime;
	public long insertionTime;
	public boolean isActive;
	
	public ByteArrayElement(long seqNo, long creationTime, long conversionTime)
	{
		super();
		seqNum = seqNo;
		this.creationTime = creationTime;
		this.conversionTime = conversionTime;
		isActive = false;
	}
	
	public void deactivate()
	{
		this.reset();
		isActive = false;
	}
	
	public int compareTo(ByteArrayElement arg0) 
	{
		if( this.seqNum < arg0.seqNum )
		{
			return -1;
		}
		else if( this.seqNum > arg0.seqNum )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
}
