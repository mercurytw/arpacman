package edu.stevens.arpac.webclient;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.renderscript.*;
import android.util.Log;

public class JpegConversionRunner 
{
	private static final String TAG = "JpegConversionRunner";
	
	private byte[] data;
	private JettyServer server;
	private long sequence = 0l;
	private Bitmap bmp;
	private boolean isInited = false;
	private boolean isTorndown = false;
	private RenderScript rsRoot;
	private int cameraWidth;
	private int cameraHeight;
	private Allocation allocationY = null;
    private Allocation allocationUV;
    private Allocation allocationOut;
    private Allocation kTableAllocation;
    private int outsize, uvamt;
    private ScriptC_YuvToRgb convScript;
    private boolean useBuffer;
    private ReentrantLock mutex;
    private boolean noFramesYet = true;
	private Resources res;
    
	public JpegConversionRunner( JettyServer server, int cameraWidth, int cameraHeight, RenderScript rsMaster, Resources res, boolean maintainRgbBuffer )
	{
		this.data = null;
		this.server = server;
		this.sequence = 0l;
		this.cameraWidth = cameraWidth;
		this.cameraHeight = cameraHeight;
		rsRoot = rsMaster;
		useBuffer = maintainRgbBuffer;
		mutex = new ReentrantLock();
		this.res = res;
		initialize();
	}
	
	private synchronized void initialize()
	{
		if( isInited == false )
		{
	    	createScript(cameraWidth, cameraHeight);
			isInited = true;
		}
	}
	
	public void teardown()
	{
		if(!isTorndown)
		{
			isTorndown = true;
		}
	}
	
    public boolean queueByteArray( Bitmap bmp, long creationTime )
    {
    	boolean res;
    	try 
    	{
    	    Bitmap bitmap = bmp;
    	    
    	    try 
    	    {
    	    	ByteArrayElement out = server.pool.get(sequence, creationTime, 0l);
	            if (res = bitmap.compress(CompressFormat.JPEG, 30, out)) 
	            {
	            	out.close();
	            	out.insertionTime = System.currentTimeMillis();
	            	out.conversionTime = out.insertionTime - creationTime;
	            	server.queue.add(out);
	            } else {
	                Log.e(TAG, "Unable to compress image");
	                out.close();
	            }
	            
	            return res;
    	    } catch (FileNotFoundException e )
    	    {
    	    	Log.e(TAG, "Couldn't open outfile for writing");
    	    	return false;
    	    } catch (IOException e) {
				Log.w(TAG, "Unable to close outstream");
				return false;
			}
    	       
    	} catch (NullPointerException e )
    	{
    		Log.e(TAG, "Name was null");
    		return false;
    	}
    }
    
    public void perform( byte[] data )
	{
    	if(isTorndown || (!server.isBound)) { return; }
    	
		this.data = data;
		sequence++;

		convert();
	}
    
    private Allocation optOut = null;
    //https://github.com/android/platform_frameworks_base/blob/master/tests/RenderScriptTests/LivePreview/src/com/android/rs/livepreview/RsYuv.java
    private void createScript(int width, int height)
    {
    	convScript = new ScriptC_YuvToRgb(rsRoot, res, R.raw.yuvtorgb);
    	// Create a new type that holds RGBA_8888's
    	Type.Builder tb = new Type.Builder(rsRoot, Element.RGBA_8888(rsRoot));
        tb.setX(width); // the type is width wide
        tb.setY(height); // and height high

        // create an output allocation reflecting the type tb.
        allocationOut = Allocation.createTyped(rsRoot, tb.create());
        // next, we're going to create an allocation for the input that's a series of
        // Int8's
        outsize = height * width;
        uvamt = (height / 2) * (width / 2) * 2;
        
        allocationY = Allocation.createSized(rsRoot, Element.I8(rsRoot), outsize);
        allocationUV = Allocation.createSized(rsRoot, Element.I8(rsRoot), uvamt);
        kTableAllocation = Allocation.createSized(rsRoot, Element.U32(rsRoot), outsize);
        // bind the k-table before invoking setSize pls
        convScript.bind_kTable(kTableAllocation);
        convScript.invoke_setSize(width, height);
    	
        convScript.set_out(allocationOut);
    	convScript.set_script(convScript);
    	bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
    	
    	if(useBuffer)
    	{
    		optOut = Allocation.createSized(rsRoot, Element.I32(rsRoot), outsize);
    	}
    }
	
	private void convert() 
	{
		long creationTime = System.currentTimeMillis();
		if( server.isTorndown() )
		{
			Log.w(TAG, "Tried to push elet to queue but server is torndown");
			return;
		}
		
    	allocationY.copy1DRangeFromUnchecked(0, outsize, data);

    	// So apparently copy1DRangeFromUnchecked is broken and off + count must be <= allocationUV.length
    	//allocationUV.copy1DRangeFromUnchecked(outsize, uvamt, data);
    	
    	// TODO: can you find a faster way to do this? Just copying it manually into a preallocated 
    	// array in a for loop is 20 ms slower than Arrays.copyOfRange. Can you copy larger chunks at a time? maybe? idk.
    	// After further investigation, you can't cast a byte[] as an int[], so no luck. Seems like your best move if 
    	// you really want to do this without allocation would be to do it natively, but I doubt the couple milliseconds
    	// of speedup you'd get would be worth it. Really, if you decide to start manipulating the camera with NDK then
    	// it'd probably be worth reconsidering
    	byte[] uvarr = Arrays.copyOfRange(data, outsize, uvamt + outsize); // stupid fucking overhead fuck fuck fuck thanks android
    	allocationUV.copy1DRangeFromUnchecked(0, uvamt, uvarr);

    	
    	convScript.set_Y(allocationY);
    	convScript.bind_UV(allocationUV);
    	
    	if(useBuffer)
    	{
    		convScript.bind_optionalOut(optOut);
    		try {
				mutex.lockInterruptibly();
				convScript.invoke_yuvToRgb();
				noFramesYet = false;
				mutex.unlock();
			} catch (InterruptedException e) {
				Log.w(TAG, "Thread interrupted while waiting for mutex");
				return;
			}
    	}
    	else
    	{
    		convScript.bind_optionalOut(null);
    		convScript.invoke_yuvToRgb();
    	}
    	
    	allocationOut.copyTo(bmp);
    	
    	
		if( bmp == null )
		{
			Log.e(TAG, "Failed to create bitmap");
		}
		else
		{
			queueByteArray( bmp, creationTime );
		}
	}
	
	public boolean getRgbBuf( byte[] inbuf )
	{
		if(!useBuffer || noFramesYet || isTorndown)
		{
			return(false);
		}
		try {
			mutex.lockInterruptibly();
			allocationOut.copyTo(inbuf);
			mutex.unlock();
		} catch (InterruptedException e) {
			Log.w(TAG, "Thread interrupted while waiting for lock");
			return false;
		}
		return true;
	}
	
	public boolean getRgbBuf( int[] inbuf )
	{
		if(!useBuffer || noFramesYet || isTorndown)
		{
			return(false);
		}
		try {
			mutex.lockInterruptibly();
			optOut.copyTo(inbuf);
			mutex.unlock();
		} catch (InterruptedException e) {
			Log.w(TAG, "Thread interrupted while waiting for lock");
			return false;
		}
		return true;
	}
}
