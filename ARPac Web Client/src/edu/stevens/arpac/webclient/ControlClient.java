package edu.stevens.arpac.webclient;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;

import org.apache.http.conn.util.InetAddressUtils;

import android.util.Log;

/**
 * A client that connects to the PTAMM server and communicates user commands over a bidirectional
 * TCP socket. Also used to get the location from PTAMM
 * @author Jeff Cochran
 *
 */
public class ControlClient
{
	private static final String TAG = "ControlClient";
	private static final int PORT_NO = 8181;
	private static final String EXIT_COMMAND = "1";
	private static final String SPC_COMMAND = "2";
	private Socket clisock;
	private boolean isTorndown;
	public boolean isBound;
	public boolean isStarted;
	private String mjpegServerAddr;
	private OutputStreamWriter writer;
	private InputStreamReader reader;
	private char[] inputBuf = new char[256];
	private ResponseListener listener;
	
	// TODO: Add in support for MJPEG Servers running on an IPv6 address
	/**
	 * Instantiates a new Control client. Note that currently the MJPEG server must be running 
	 * bound to an IPv4 address
	 * @param mjpegServerAddr The dot-delimited IPv4 address of the MJPEG server. 
	 */
	public ControlClient(String mjpegServerAddr)
	{
		isTorndown = false;
		isBound = false;
		isStarted = false;
		this.mjpegServerAddr = mjpegServerAddr;
		listener = new ResponseListener();
	}
	
	/**
	 * Connects to the server at the specified hostname & port
	 * @param host The hostname of the server. may be a dot-delimited IPv4 address for example
	 * @param port port number to connect to
	 * @throws IOException If we were unable to bind to the server for network reasons, or connect to the internet
	 * @throws NumberFormatException If the port number passed in was not a parseable number
	 * @throws UnknownHostException If we couldn't resolve the hostname passed in
	 */
	public void connect(String host, String port) throws NumberFormatException, IOException, UnknownHostException
	{
		InetAddress localAddr = getIPAddress();
		if( localAddr == null )
		{
			Log.e(TAG, "Unable to find appropriate non-loopback address.");
			throw new IOException("Unable to find appropriate non-loopback address");
		}
		InetAddress serverAddr = InetAddress.getByName(host);
		clisock = new Socket(serverAddr, Integer.valueOf(port), localAddr, PORT_NO);
		clisock.setKeepAlive(true);
		if(!clisock.isConnected())
		{
			Log.e(TAG, "socket isn't connected???");
		}
		writer = new OutputStreamWriter(clisock.getOutputStream());
		reader = new InputStreamReader(clisock.getInputStream());
		listener.setReader(reader);
		isBound = true;
		Log.v(TAG, "Successfully bound control socket");
	}
	
	/**
	 * Begin sending video to the PTAMM server
	 */
	public void start()
	{
		if(isTorndown || !isBound)
		{
			Log.w(TAG, "Start called in invalid state");
			return;
		}
		try {
			writer.write(mjpegServerAddr);
			writer.flush();
			for(int charsRead = 0; charsRead < 3; )
			{
				if( isTorndown )
				{
					return;
				}
				int read = reader.read(inputBuf, 0, 3);
				if(read == -1)
				{
					Log.e(TAG, "start: Encountered end of stream");
					return;
				}
				charsRead += read;
			}
		} catch (IOException e) {
			Log.e(TAG, "Failed to send MJPEG server IP due to network failure: " + e.getMessage());
			return;
		}
		listener.start();
		isStarted = true;
	}
	
	public void teardown()
	{
		if(!isTorndown)
		{
			isTorndown = true;
			if(isBound)
			{
				if( isStarted )
				{
					listener.teardown();
					listener.interrupt();
					try {
						writer.write(EXIT_COMMAND);
						writer.flush();
						try {
							Thread.sleep(300);
						} catch (InterruptedException e) {
							// do nothing
						}
					} catch (IOException e) {
						Log.w(TAG, "teardown: Failed to send exit command to PTAMM server");
					}
					isStarted = false;
				}
				try {
					clisock.close();
					// This will forcibly close the response listener thread by means of
					// an IOException. It's kind of hacky, but otherwise I'd have to do fancy stuff
					// to interupt the blocking thread.
					try {
						listener.join();
					} catch (InterruptedException e) {
						Log.w(TAG, "Couldn't wait for the response listener thread to join");
					}
				} catch (IOException e) {
					Log.w(TAG, "Unable to close the client socket. whatevs.");
				}
				
				isBound = false;
			}
		}
	}
	
	public void captureInitFrame()
	{
		if( isTorndown || !isStarted || listener.isInitialized ) { return; }
		try {
			writer.write( SPC_COMMAND );
			writer.flush();
		} catch (IOException e) {
			Log.e(TAG, "Network error: " + e.getMessage());
			return;
		}
	}
	
	public boolean isPtammInitialized()
	{
		return listener.isInitialized;
	}
	
	public Vector3 getPlayerPosition()
	{
		if( isTorndown || !isStarted || !listener.isInitialized ) { return null; }
		return listener.getPosition();
	}
	
	public Vector3 getPlayerOrientation()
	{
		if( isTorndown || !isStarted || !listener.isInitialized ) { return null; }
		return listener.getOrientation();
	}
	
	public int getTrackingQuality()
	{
		if( isTorndown || !isStarted || !listener.isInitialized ) { return -1; }
		return listener.getTrackingQuality();
	}
	
	public int getCameraPose(float[] posBuff, int poffset, float[] lieBuff, int loffset)
	{
		if( isTorndown || !isStarted || !listener.isInitialized ) { return -1; }
		return listener.getCameraPose(posBuff, poffset, lieBuff, loffset);
	}
	
	// TODO: do we really want to disallow IPv6 and mobile addresses here? 
	/**
     * Get address from first non-localhost interface
     * @return  address or empty string
     */
    private InetAddress getIPAddress() 
    {
        try 
        {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) 
            {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) 
                {
                    if (!addr.isLoopbackAddress())
                    { 
                    	String sAddr = addr.getHostAddress().toUpperCase(); 
                        if (InetAddressUtils.isIPv4Address(sAddr))
                        {
                        	if(addr.getAddress()[0] != ((byte)10))
                        	{
                        		Log.i(TAG, "Got address " + addr.toString());
                        		return addr;
                        	}
                        	else
                        	{
                        		Log.i(TAG, "Caught a mobile address");
                        	}
                        } 
                    	
                    }
                }
            }
        } 
        catch (Exception ex) 
        { 
        	Log.e(TAG, "could not get IP address: " + ex.getMessage());
        } // for now eat exceptions
        Log.e(TAG, "Could not find a non-loopback IP address!");
        return null;
    }
}
