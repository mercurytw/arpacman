package edu.stevens.arpac.webclient;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import android.content.Context;
import android.content.res.Resources;
import android.hardware.Camera;
import android.renderscript.RenderScript;
import android.util.Log;

/**
 * This is the root object of the webclient package. It handles all communication with the 
 * PTAMM server backend, and image manipulation.
 * @author Jeff Cochran
 *
 */
public final class PtammLocationProvider 
{
	private static final String TAG = "PtammLocationProvider";
	public enum PtammState { NOT_CONNECTED, READY, INITIALIZED, DONE };
	private PtammState currentState = PtammState.NOT_CONNECTED;
	private enum supportedFormat { NV21, JPEG, NONE };
	private long framerate;
	private JettyServer server;
	private ControlClient control = null;
	private JpegConversionRunner runner = null;
	private boolean isTorndown = false;
	private AtomicBoolean ready;
	private AtomicBoolean isWorkPending;
	private int width, height;
	private boolean workerShouldJoin = false;
	private byte[] image;
	private Camera cam;
	private String addr;
	
	private Thread jpegExecutor = new Thread("JpegExecutor") {
		@Override
		public void run()
		{
			while(!workerShouldJoin)
			{
				if( isWorkPending.get() == true )
				{
					runner.perform(image);
					isWorkPending.set(false);
					ready.set(true);
					in = false;
					cam.addCallbackBuffer(image);
				}
				else
				{
					Thread.yield();
				}
			}
		}
	};
	
	/**
	 * Constructs a new PtammLocationProvider. Note that once this is instantiated, any changes to the camera's preview
	 * width or height will break this system. If you wish to use the rgbBuf parameter, you must lock PtammLocationProvider's 
	 * rgbBufferMutex when accessing the data.
	 * @param context The application context
	 * @param serverAddr an IPv4 string containing the address of the PTAMM server
	 * @param previewWidth The width of the preview image
	 * @param previewHeight The height of the preview image
	 * @param framerateMillis The framerate in milliseconds (for optimization). currently, the code handles a framerate of
	 * about 1 frame every 32 millis at best. Setting the code to run faster could cause memory management overhead. 
	 * @param res A {@link android.content.res.Resources Resource} instance from the main activity (e.g., from a call to {@link android.content.ContextWrapper#getResources() getResources()})
	 * @param maintainRgbBuffer if true, PtammLocationProvider will provide access to a byte[] which can be accessed to retrieve the latest
	 * camera frame in RGBA8888. Note that the frame will be updated up to the frequency of the framerateMillis field (or less depending on
	 * hardware conversion speed). Setting this to true will incur overhead.
	 */
	public PtammLocationProvider(Context context, String serverAddr, int previewWidth, int previewHeight, long framerateMillis, Resources res, boolean maintainRgbBuffer)
	{
		server = new JettyServer();
        server.start();
		RenderScript rs = RenderScript.create(context);
		ready = new AtomicBoolean(true);
		isWorkPending = new AtomicBoolean(false);
		framerate = framerateMillis;
		width = previewWidth;
		height = previewHeight;
		runner = new JpegConversionRunner(server, width, height, rs, res, maintainRgbBuffer);
		jpegExecutor.start();
		int size = (previewHeight * previewWidth) + ((previewHeight / 2) * (previewWidth / 2) * 2);
        image = new byte[size];
		this.addr = serverAddr;
		// TODO: learn how to do cross-thread exception handling
		Thread t = new Thread() {
			@Override
			public void run()
			{
				while(!server.isStarted)
				{
					if(isTorndown)
					{
						return;
					}
					Thread.yield();
				}
				control = new ControlClient(server.getServerAddress());
				try {
					control.connect(addr, "8181");
				} catch (NumberFormatException e) {
					Log.e(TAG, "bad format: " + e.getMessage());
				} catch (UnknownHostException e) {
					Log.e(TAG, "Bad hostname: " + e.getMessage());
				} catch (IOException e) {
					Log.e(TAG, "No internet: " + e.getMessage());
				}
			}
		};
		t.start();
	}
	
	/**
	 * If maintainRgbBuffer was set to true when this class was instantiated, this function can
	 * be called to populate the input parameter with the latest processed camera frame. This call may block for thread synchronization.
	 * @param inbuf a byte[] of the size preview width * preview height
	 * @return a true on success or false if <br><ul>
	 * <li>maintainRgbBuffer was not set to true</li>
	 * <li>this function was called in an invalid state</li>
	 * <li>the first frame of video has not yet been processed</li>
	 * </ul>
	 */
	public boolean getRgba8888( byte[] inbuf )
	{
		if( isTorndown )
		{
			return false;
		}
		return runner.getRgbBuf( inbuf );
	}
	
	public boolean getRgba8888( int[] inbuf )
	{
		if( isTorndown )
		{
			return false;
		}
		return runner.getRgbBuf( inbuf );
	}
	
	public boolean isPtammInitialized()
	{
		if( isTorndown || !control.isStarted )
		{
			return false;
		}
		return(control.isPtammInitialized());
	}
	
	public PtammState getState()
	{
		if(currentState == PtammState.READY)
		{
			if(control.isPtammInitialized())
			{
				currentState = PtammState.INITIALIZED;
			}
		}
		return currentState;
	}
	
	/**
	 * Gets the player's current position as a {@link Vector3}. This function may block for a short time,
	 * so this function may be best called in a thread other than the UI thread.
	 * @return A Vector3 or null if the server has not yet reported 
	 * the player's first position or if this function was called in an invalid state (i.e., 
	 * the server has not been connected or PTAMM initialized yet).
	 */
	public Vector3 getPlayerPosition()
	{
		if( isTorndown || !control.isPtammInitialized() ) { return null; }
		return control.getPlayerPosition();
	}
	
	/**
	 * Gets the player's current orientation as a {@link Vector3} in the Special Orthogonal Lie group. 
	 * This function may block for a short time, so this function may be best called in a thread other than the UI thread.
	 * @return A Vector3 or null if the server has not yet reported 
	 * the player's first position or if this function was called in an invalid state (i.e., 
	 * the server has not been connected or PTAMM initialized yet).
	 */
	public Vector3 getPlayerOrientation()
	{
		if( isTorndown || !control.isPtammInitialized() ) { return null; }
		return control.getPlayerOrientation();
	}
	
	/**
	 * Gets the quality of map tracking so far as an int between 0 and 3, or -1 if an error occurs. 
	 * This function may block for a short time, so this function may be best called in a thread other than the UI thread.
	 * @return An int representing tracking quality between 0 and 3 or -1 if the server has not yet reported 
	 * the player's first position or if this function was called in an invalid state (i.e., 
	 * the server has not been connected or PTAMM initialized yet).
	 */
	public int getTrackingQuality()
	{
		if( isTorndown || !control.isPtammInitialized() ) { return -1; }
		return control.getTrackingQuality();
	}
	
	/**
	 * Gets the player's current position in the world, and orientation in the 
	 * Special Orthogonal Lie group, stored in a float buffer.
	 * This function may block for a short time, so this function may be best 
	 * called in a thread other than the UI thread.
	 * @param posBuff a float buffer in which to store the position
	 * @param poffset the index into the buffer at which to store the position
	 * @param lieBuff a float buffer in which to store the lie
	 * @param loffset the index into the buffer at which to store the lie
	 * @return an int representing tracking quality, or -1 if the server has not yet reported 
	 * the player's first position or if this function was called in an invalid state (i.e., 
	 * the server has not been connected or PTAMM initialized yet).
	 */
	public int getCameraPose(float[] posBuff, int poffset, float[] lieBuff, int loffset)
	{
		if( isTorndown || !control.isPtammInitialized() ) { return -1; }
		return control.getCameraPose(posBuff, poffset, lieBuff, loffset);
	}
	
	public void teardown()
	{
		if( !isTorndown )
		{
			isTorndown = true;
			currentState = PtammState.DONE;
			control.teardown();
			workerShouldJoin = true;
			
			server.teardown();
        	server.dumpProfile();
        	
        	runner.teardown();
        	try {
				jpegExecutor.join();
			} catch (InterruptedException e) {
				Log.w(TAG, "interrupted while waiting for the worker thread to join.");
			}
        	Log.i(TAG, "torndown");
		}
	}
	
	private supportedFormat checkSupportedPreviewFormats( Camera.Parameters params )
    {
		@SuppressWarnings("unused")
		boolean jpeg = false;
		boolean nv21 = false;
		List<Integer> l = params.getSupportedPreviewFormats();
		for ( int i = 0; i < l.size(); i++ )
		{
			switch( l.get(i) )
			{
			case android.graphics.ImageFormat.JPEG: 
				jpeg = true; break;
			case android.graphics.ImageFormat.NV21:
				nv21 = true; break;
			default:
				break;
			}
		}
		// TODO: build in support for if the camera supports JPEG preview format
		//if(jpeg) { return supportedFormat.JPEG; }
		if(nv21) { return supportedFormat.NV21; }
		return supportedFormat.NONE;
    }
	
	/**
	 * Set up your camera's parameters for use with this library.
	 * @param cam The camera whose parameters we will modify.
	 * @return false if the android device we are working on will not work with this library.
	 */
	public boolean setupCamera(Camera cam)
	{
		Camera.Parameters params = cam.getParameters();
		if(checkSupportedPreviewFormats(params) == supportedFormat.NONE)
		{
			Log.e(TAG, "This android device supports no valid preview formats! This library WILL NOT WORK");
			return false;
		}
		params.setPreviewFormat(android.graphics.ImageFormat.NV21);
		cam.setParameters(params);
		cam.addCallbackBuffer(image);
		this.cam = cam;
		return true;
	}
	
	@SuppressWarnings("unused")
	private void changeCameraParameters(Camera.Parameters params)
	{
		// TODO: implement me using a reentrant lock or something.
		// just instantiate a new JpecConversionRunner. Shouldn't be terrible
	}
	
	private AtomicBoolean doOnce = new AtomicBoolean(false);
	private void startControl()
	{
		if(doOnce.compareAndSet(false, true) == true)
		{
			Thread t = new Thread() {
				@Override
				public void run()
				{
					while(!control.isBound)
					{
						if(isTorndown)
						{
							return;
						}
						Thread.yield();
					}
					control.start();
					currentState = PtammState.READY;
					cam.addCallbackBuffer(image);
				}
			};
			t.start();
		}
	}
	
	private boolean in = false;
	private long timeBefore = 0l;
	/**
	 * This function will send a preview frame to the PTAMM server for
	 * vision processing. It should be called inside a preview callback method of a
	 * {@link android.hardware.Camera.PreviewCallback Camera.PreviewCallback}. 
	 */
	public void processFrame()
	{
		if((isTorndown) || (control == null))
		{
			cam.addCallbackBuffer(image);
			return;
		}
		if(!control.isStarted)
		{
			startControl();
			return;
		}
		long now = System.currentTimeMillis();
		if( ( now - timeBefore >= framerate ) && (ready.compareAndSet(true, false) == true ) )
		{	
			if(isWorkPending.compareAndSet(false, true) == true )
			{
				timeBefore = now;
				in = true;
				ready.set(true);
			}
		}
		else if( in == false )
		{
			cam.addCallbackBuffer(image);
		}
	}
	
	/**
	 * Capture a frame that will be used to initialize PTAMM
	 */
	public void captureInitFrame()
	{
		if( isTorndown || !control.isStarted ) { return; }
		control.captureInitFrame();
	}
	
	
	// TODO: write onPause, onResume, and onDestroy methods
	public void onPause()
	{
		
	}
	
	public void onResume()
	{
		
	}
	
	public void onDestroy()
	{
		
	}
}
