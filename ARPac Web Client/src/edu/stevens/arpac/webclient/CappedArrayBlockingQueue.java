package edu.stevens.arpac.webclient;

import java.util.Collection;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import android.util.Log;

/**
 * An ArrayBlockingQueue, except it's size is capped, deleting the head
 * of the queue when the max size is reached. 
 * @author jcochran
 *
 * @param <E> class of the elements the queue contains
 */
public class CappedArrayBlockingQueue<E> extends ArrayBlockingQueue<E> 
{

	/**
	 * what is this I don't even
	 */
	private static final long serialVersionUID = -3993678640010786395L;
	private static final String TAG = "CappedArrayBlockingQueue";
	private int capacity;
	private ReentrantLock mutex;
	
	public CappedArrayBlockingQueue(int capacity) {
		super(capacity);
		this.capacity = capacity;
		mutex = new ReentrantLock();
	}
	
	public CappedArrayBlockingQueue(int capacity, boolean fair)
	{
		super(capacity, fair);
		this.capacity = capacity;
		mutex = new ReentrantLock();
	}

	public CappedArrayBlockingQueue(int capacity, boolean fair, Collection<? extends E> c) {
		super(capacity, fair, c);
		this.capacity = capacity;
		mutex = new ReentrantLock();
	}

	private void checkForCap()
	{
		while( this.size() > capacity )
		{
			try {
				this.take();
			} catch (InterruptedException e) {
				Log.e(TAG, "Thread interrupted while popping in checkForCap");
			}
		}
	}

	@Override
	public boolean add(E e)
	{
		boolean result;
		try {
			mutex.lockInterruptibly();
		} catch (InterruptedException e1) {
			return(true);
		}
		checkForCap();
		result = super.add(e);
		mutex.unlock();
		return(result);
	}
	
	@Override
	public boolean offer(E e)
	{
		boolean result;
		try {
			mutex.lockInterruptibly();
		} catch (InterruptedException e1) {
			return(true);
		}
		checkForCap();
		result = super.offer(e);
		mutex.unlock();
		return(result);
	}
	
	@Override
	public boolean offer(E e, long timeout, TimeUnit unit) throws InterruptedException
	{
		boolean result;
		try {
			mutex.lockInterruptibly();
		} catch (InterruptedException e1) {
			return(true);
		}
		checkForCap();
		try {
			result = super.offer(e, timeout, unit);
		} catch (InterruptedException e1) {
			mutex.unlock();
			throw e1;
		}
		mutex.unlock();
		return(result);
	}
	
	@Override
	public void put(E e) throws InterruptedException
	{
		try {
			mutex.lockInterruptibly();
		} catch (InterruptedException e1) {
			return;
		}
		checkForCap();
		try {
			super.put(e);
		} catch (InterruptedException e1) {
			mutex.unlock();
			throw e1;
		}
		mutex.unlock();
	}

}
