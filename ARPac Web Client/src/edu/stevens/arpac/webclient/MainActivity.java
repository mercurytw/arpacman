package edu.stevens.arpac.webclient;

import java.util.List;

import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener 
{
	private static final String TAG ="MainActivity";
	
	private Camera mCamera;
    private CameraPreview mPreview;
    private MediaRecorder mMediaRecorder;
    private Button mButton;
    private Button mSpaceButton;
    private TextView mPositionText;
    private TextView mStateText;
    private Handler mHandler;
    private Runnable r = new Runnable() {
		public void run() {
			if(mPreview.locProv != null)
			{
				PtammLocationProvider.PtammState state = mPreview.locProv.getState();
				switch(state) 
				{
				case INITIALIZED:
					mStateText.setText("I");
					break;
				case READY:
					mStateText.setText("R");
					break;
				case DONE:
					mStateText.setText("D");
					break;
				default:
					break;
				}
				if((mPreview.locProv.isPtammInitialized()))
				{
					Vector3 pos = mPreview.locProv.getPlayerPosition();
					if( pos != null )
					{
						String s = pos.toString();
						mPositionText.setText(s);		
					}
				}
			}
			mHandler.postDelayed(r, 1000l);
		}
	};
    private View.OnClickListener mSpaceListener = new View.OnClickListener() {
		public void onClick(View arg0) {
			mPreview.spaceClick();
		}
    };

    private String checkSupportedPreviewFormats( Camera c )
    {
    	String ostring = "";
    	 List<Integer> l = c.getParameters().getSupportedPreviewFormats();
         String s = new String();
         for ( int i = 0; i < l.size(); i++ )
         {
        	if( i != 0 )
        	{
        		ostring += ", ";
        	}
         	switch( l.get(i) )
         	{
         	case android.graphics.ImageFormat.JPEG: 
         		s = "JPEG"; break;
         	case android.graphics.ImageFormat.NV16:
         		s = "NV16"; break;
         	case android.graphics.ImageFormat.NV21:
         		s = "NV21"; break;
         	case android.graphics.ImageFormat.RGB_565:
         		s = "RGB_565"; break;
         	case android.graphics.ImageFormat.UNKNOWN:
         		s = "UNKNOWN"; break;
         	case android.graphics.ImageFormat.YUY2:
         		s = "YUY2"; break;
         	case android.graphics.ImageFormat.YV12:
         		s = "YV12"; break;
         		default:
         			s = "unsupported unknown";
         	}
         	ostring += s;
         }
         return ostring;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCamera = getCameraInstance();
        if(mCamera == null)
        {
        	Log.e(TAG, "Failed to get the camera");
        }
        Log.i(TAG, "Supported formats: " + checkSupportedPreviewFormats( mCamera ));

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera, getResources());
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview); 
        preview.addView(mPreview); 
        
     // Add a listener to the Capture button
        mButton = (Button) findViewById(R.id.button_capture);
        mButton.setOnClickListener( this );
        mSpaceButton = (Button) findViewById(R.id.button_space);
        mSpaceButton.setOnClickListener( mSpaceListener );
        mPositionText = (TextView) findViewById(R.id.text_position);
        mStateText = (TextView) findViewById(R.id.text_state);
        mHandler = new Handler();
        
        getResources();
    }
    
    private boolean isStarted = false;
    private void ryoMjpegServer()
    {
    	if( !isStarted )
    	{
    		//mCamera.setPreviewCallback(mPreview);
    		mCamera.setPreviewCallbackWithBuffer(mPreview);
    		mHandler.postDelayed(r, 1000l);
    		isStarted = true;
    	}
    	else
    	{
    		mPreview.stop();
    		isStarted = false;
    	}
    }
    
    public void onClick(View v) 
    {
    	ryoMjpegServer();
    }
    
    public void setCaptureButtonText(String s)
    {
    	Button captureButton = (Button) findViewById(R.id.button_capture);
    	captureButton.setText(s);
    }
    
    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        Camera.CameraInfo info = new Camera.CameraInfo();
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) 
        {
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) 
            {
            	try 
            	{
            		c = Camera.open(i);                 
            	}
            	catch (Exception e)
                {
                	// Camera is not available (in use or does not exist)
                	Log.e(TAG, "Could not access camera: " + e.getMessage());
                }
            }    
        }
        return c; // returns null if camera is unavailable
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        //mTimer.cancel();
        mHandler.removeCallbacks(r);
        releaseMediaRecorder();
        releaseCamera();         // release the camera immediately on pause event
        mPreview.stop();
        
    }
    
    @Override
    protected void onResume()
    {
    	super.onResume();
    	//mCamera = getCameraInstance();
    }
    
    @Override
    protected void onDestroy()
    {
    	super.onDestroy();
    }
    
    private void releaseMediaRecorder(){
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();   // clear recorder configuration
            mMediaRecorder.release(); // release the recorder object
            mMediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }
}
