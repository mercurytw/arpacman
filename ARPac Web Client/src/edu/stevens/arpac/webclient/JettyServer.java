package edu.stevens.arpac.webclient;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.conn.util.InetAddressUtils;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.Request;

import com.oreilly.servlet.MultipartResponse;

import android.util.Log;
// TODO: Test me for network failure resilience
// holla at http://puregeekjoy.blogspot.com/2011/06/running-embedded-jetty-in-android-app.html
/**
 * Lean and mean Jetty HTTP server which has be implemented for use as an MJPEG server.
 * @author jcochran
 *
 */
public class JettyServer extends Thread 
{
	private static final String TAG = "JettyServer";
	private static final int PORT_NO = 8080;
	private static final int MAX_QUEUE_CAPACITY = 256;
	private static final int INITIAL_POOL_SIZE = 32;
	private Server webServer;
	public Boolean isStarted = false;
	public boolean isBound = false;
	private Boolean __isTorndown = false;
	protected CappedArrayBlockingQueue<ByteArrayElement> queue;
	private boolean finished = true;
	protected ByteArrayElementPool pool = null;
	private long buffersWritten = 0l;
	private long totalConversionTime = 0l;
	private long totalTime = 0l;
	private long totalInsertionTime = 0l;
	private long totalWriteTime = 0l;
	private InetSocketAddress serverAddr;
	private String serverIP;
	
	/**
	 * Instantiates a new JettyServer.
	 */
	public JettyServer()
	{
		super();
		queue = new CappedArrayBlockingQueue<ByteArrayElement>(MAX_QUEUE_CAPACITY);
		pool = new ByteArrayElementPool( INITIAL_POOL_SIZE );
	}
	
	public void teardown()
	{
		if(__isTorndown) { return; }

		__isTorndown = true;
		if( isStarted )
		{
			while( !finished )
			{
				Thread.yield();
			}
			try {
				webServer.stop();
				isStarted = false;
			} catch (Exception e) {
				Log.e(TAG, "Couldn't stop server. Probably was called when server already stopped.");
			}
		}
		queue.clear();
	}

	public Boolean isTorndown()
	{
		return __isTorndown;
	}
	
	public String getServerAddress()
	{
		if(__isTorndown || !isStarted)
		{
			return null;
		}
		return serverIP;
	}
	
	private long bindTime = 0l;
	private boolean initServer() throws IOException
	{
		Log.i(TAG, "Initializing server to port " + PORT_NO);
		serverAddr = getIPAddress();
		if( serverAddr == null )
		{
			Log.e(TAG, "Failed to initialize server: Could not find IPv4 non-loopback interface");
			throw new IOException("Possibly not connected to the internet.");
		}
		webServer = new Server(serverAddr);
		serverIP = serverAddr.getAddress().getHostAddress();
		
		Handler handler = new AbstractHandler() {
            public void handle(String target, Request request, HttpServletRequest servletRequest,
                    HttpServletResponse servletResponse) throws IOException, ServletException {
          
            	ServletOutputStream out = servletResponse.getOutputStream();
            	 
            	 MultipartResponse multi = new MultipartResponse(servletResponse);
            	 isBound = true;
            	 Log.v(TAG, "PTAMM server bound");
            	 long before, after;
            	 Boolean go = true;
            	 ByteArrayElement elet;
            	 bindTime = System.currentTimeMillis();
            	 while( go )
            	 {
            		 
            		 if( __isTorndown )
            		 {
            			multi.finish();
            			request.setHandled(true);
            			Log.i(TAG, "End of Stream");
            			finished = true;
            			isBound = false;
            			return;
            		 }
            		 try {
            			 elet = null;
            			 while( ( elet = queue.poll(500l, TimeUnit.MILLISECONDS) ) == null )
            			 {
            				 if(__isTorndown)
            				 {
            					 multi.finish();
            					 request.setHandled(true);
            					 finished = true;
            					 isBound = false;
            					 return;
            				 }
            				 Thread.yield();
            			 }
      					before = System.currentTimeMillis();
      				} catch (InterruptedException e) {
      					Log.w(TAG, "Thread interrupted while waiting to take an elet off the queue");
      					multi.finish();
      					request.setHandled(true);
      					finished = true;
      					isBound = false;
      					return;
      				}
            		 try
            		 {
            			 multi.startResponse("image/jpeg");
            			 out.write( elet.toByteArray(), 0, elet.size() );
            			 multi.endResponse();
            		 }
            		 catch(IOException ex)
            		 {
            			go = false;
            			if( ex.getMessage() != null )
            			{
            				Log.e(TAG, "IO Failed with exception " + ex.getMessage());
            			}
            			else
            			{
            				Log.i(TAG, "MJPEG stream client disconnected before multi.finish");
            			}
            		 }
            		 // profiling
            		 if( elet.creationTime >= bindTime )
            		 {
            			 after = System.currentTimeMillis();
            			 totalConversionTime += elet.conversionTime;
            			 totalInsertionTime += before - elet.insertionTime;
            			 totalWriteTime += after - before;
            			 totalTime += after - elet.creationTime;
            			 buffersWritten++;
            		 }
            		 if( elet != null )
            		 {
            			pool.reclaim(elet);
            		 }
            	 }
            	 isBound = false;
            	 request.setHandled(true);
            	 finished = true;
            }
        };
        webServer.setHandler(handler);

        try {
            webServer.start();
            Log.d(TAG, "started Web server @ " + serverAddr.toString());
            isStarted = true;
            
        }
        catch (Exception e) {
            Log.d(TAG, "unexpected exception starting Web server: " + e);
            return(false);
        }
        return(true);
	}
	
	/**
	 * Dump profiling information if available.
	 */
	public void dumpProfile()
	{
		if( buffersWritten <= 0 )
		{
			Log.i(TAG, "No buffers written.");
			return;
		}
		Log.i(TAG, "Wrote " + Long.toString(buffersWritten) + " buffers");
		Log.i(TAG, "Average Conversion Time: " + Long.toString(totalConversionTime / buffersWritten) + " ms");
		Log.i(TAG, "Average Time in Queue: " + Long.toString(totalInsertionTime / buffersWritten) + " ms");
		Log.i(TAG, "Average Time Writing to Stream: " + Long.toString(totalWriteTime / buffersWritten) + " ms");
		Log.i(TAG, "Average Turnaround Time: " + Long.toString(totalTime / buffersWritten) + " ms");
	}
	
	/**
     * Get IPv4 address from first non-localhost interface
     * @return  address or empty string
     */
    private InetSocketAddress getIPAddress() 
    {
    	InetSocketAddress sockaddr = null;
    	InetAddress mobileAddr = null;
        try 
        {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) 
            {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) 
                {
                    if (!addr.isLoopbackAddress())
                    {
                        String sAddr = addr.getHostAddress().toUpperCase(); 
                        if (InetAddressUtils.isIPv4Address(sAddr))
                        {
                        	if(addr.getAddress()[0] != ((byte)10))
                        	{
                        		sockaddr = new InetSocketAddress(addr, PORT_NO);
                        		return sockaddr;
                        	}
                        	else
                        	{
                        		mobileAddr = addr;
                        	}
                        } 
                    }
                }
            }
        } 
        catch (Exception ex) 
        { 
        	Log.e(TAG, "could not get IP address: " + ex.getMessage());
        } // for now eat exceptions
        if(mobileAddr != null)
        {
        	Log.i(TAG, "Using a mobile IP address");
        	return new InetSocketAddress(mobileAddr, PORT_NO);
        }
        Log.e(TAG, "Could not find a non-loopback IPv4 address!");
        return sockaddr;
    }
	
	public void run() 
	 {
		try {
			initServer();
		} catch (IOException e) {
			Log.e(TAG, "Couldn't start server: " + e.getMessage());
		}
	 }
}
