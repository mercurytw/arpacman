package edu.stevens.arpac.webclient;

import java.io.IOException;

import android.content.Context;
import android.content.res.Resources;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/** A basic Camera preview class */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback 
{
    @SuppressWarnings("unused")
	private static final float MILLIS_TO_SECONDS = 0.001f;
    private static final int WIDTH = 320;
    private static final int HEIGHT = 240;
	private static final String TAG = "CameraPreview";
    
    
    private SurfaceHolder mHolder;
    private Camera mCamera;
 
    private boolean isStopped = false;
    public PtammLocationProvider locProv = null;
    private Context ctx;
    public byte[] previewBuffer;
    private Resources res;

    @SuppressWarnings("deprecation")
	public CameraPreview(Context context, Camera camera, Resources res) {
        super(context);
        mCamera = camera;
        ctx = context;
        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        
        int size = (HEIGHT * WIDTH) + ((HEIGHT / 2) * (WIDTH / 2) * 2);
        previewBuffer = new byte[size];
        
        this.res = res;
    }
    
    public void stop()
    {
    	if(!isStopped)
    	{
    		isStopped = true;
    		locProv.teardown();
    	}
    }
    
    public void spaceClick()
    {
    	if(!isStopped)
    	{
    		locProv.captureInitFrame();
    	}
    }

    public void surfaceCreated(SurfaceHolder holder) {
    	Log.d(TAG, "Surface created");
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d(TAG, "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) 
    {    
    	//mCamera.setPreviewCallback( null );
    	stop();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.
    	Log.d(TAG, "Surface changed");
        if (mHolder.getSurface() == null){
          // preview surface does not exist
          return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
          // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try 
        {
        	mCamera.setPreviewDisplay(mHolder);
        	Camera.Parameters params = mCamera.getParameters();
        	params.setPreviewSize( 320, 240 );
        	mCamera.setParameters(params);
        } catch (IOException e){
        	Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }

        if( locProv == null )
        {
        	Camera.Size siz = mCamera.getParameters().getPreviewSize();
        	locProv = new PtammLocationProvider(ctx, "192.168.1.135", siz.width, siz.height, 32l, res, false);
        }
        if(!locProv.setupCamera(mCamera))
        {
        	Log.e(TAG, "Android device incompatible with webclient library");
        	this.stop();
        	return;
        }
        mCamera.startPreview();

        
    }
    
	public void onPreviewFrame(byte[] data, Camera camera) 
	{
		if(isStopped)
		{
			return;
		}
		locProv.processFrame();
	}
}