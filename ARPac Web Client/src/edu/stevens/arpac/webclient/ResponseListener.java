package edu.stevens.arpac.webclient;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.locks.ReentrantLock;

import android.util.Log;

/**
 * A thread that handles responses from the PTAMM server
 * @author Jeff Cochran
 *
 */
public class ResponseListener extends Thread 
{
	private static final String TAG = "ResponseListener";
	private static final char PTAMM_INITIALIZED = 'i';
	private InputStreamReader reader;
	public boolean isInitialized = false;
	private boolean isTorndown = false;
	private Vector3 position = null;
	private Vector3 lie = null;
	private int trackingQuality = 0;
	private ReentrantLock mutex;
	private long totalThroughputTimeMillis = 0l;
	private long posesSeen = 0l;
	
	public ResponseListener()
	{
		super("ResponseListener");
		this.reader = null;
		mutex = new ReentrantLock(true);
	}
	
	public ResponseListener(InputStreamReader reader)
	{
		super("ResponseListener");
		this.reader = reader;
		mutex = new ReentrantLock(true);
	}
	
	/**
	 * Set the InputStreamReader of the connected socket. Use this if you want to instantiate an object
	 * of this class before you're connected to a socket.
	 * @param reader An InputStreamReader of a socket
	 */
	public void setReader(InputStreamReader reader)
	{
		this.reader = reader;
	}
	
	public void teardown()
	{
		if(!isTorndown)
		{
			isTorndown = true;
			if(posesSeen != 0l)
			{
				Log.i(TAG, "Average Throughput Time = " + (totalThroughputTimeMillis / posesSeen) + " ms");
			}
		}
	}
	
	private void checkInit(char []buf)
	{
		int amt;
		try {
			amt = reader.read(buf, 0, 1);
			if(amt == 1)
			{
				if(buf[0] == PTAMM_INITIALIZED)
				{
					isInitialized = true;
					Log.i(TAG, "PTAMM is initialized successfully");
				}
			}
		} catch(IOException e) {
			// Downgraded from an error to a warning because I use this 
			// to stop the thread by closing the socket
			Log.w(TAG, "Failed to read response from socket: " + e.getMessage());
		}
	}
	
	private void setPose(float x, float y, float z, float a, float b, float c, int q)
	{
		try {
			mutex.lockInterruptibly();
		} catch (InterruptedException e) {
			Log.d(TAG, "Interrupted while waiting for lock");
			return;
		}
		if(position == null)
		{
			position = new Vector3(x, y, z);
			Log.d(TAG, "Got first position: " + position);
		}
		else
		{
			position.x = x;
			position.y = y;
			position.z = z;
		}
		
		if(lie == null)
		{
			lie = new Vector3(a, b, c);
			Log.d(TAG, "Got first orientation: " + lie);
		}
		else
		{
			lie.x = a;
			lie.y = b;
			lie.z = c;
		}
		trackingQuality = q;
		mutex.unlock();
	}
	 
	/**
	 * Gets the latest position reported by the server. Blocks
	 * @return Latest position as a Vector3
	 */
	public Vector3 getPosition()
	{
		try {
			mutex.lockInterruptibly();
		} catch (InterruptedException e) {
			Log.d(TAG, "Interrupted while waiting for lock RETURNING NULL");
			return null;
		}
		if(position == null )
		{
			mutex.unlock();
			return null;
		}
		Vector3 ret = new Vector3(position);
		mutex.unlock();
		return ret;
	}
	
	/**
	 * Gets the latest orientation reported by the server. Blocks
	 * @return Latest orientation as a Vector3 in the Special Orthogonal Lie space
	 */
	public Vector3 getOrientation()
	{
		try {
			mutex.lockInterruptibly();
		} catch (InterruptedException e) {
			Log.d(TAG, "Interrupted while waiting for lock RETURNING NULL");
			return null;
		}
		if(lie == null )
		{
			mutex.unlock();
			return null;
		}
		Vector3 ret = new Vector3(lie);
		mutex.unlock();
		return ret;
	}
	
	/**
	 * Returns tracking quality on a scale from 0-3, 0 being lost
	 * @return an int representing tracking quality, or -1 if we something went wrong
	 */
	public int getTrackingQuality()
	{
		try {
			mutex.lockInterruptibly();
		} catch (InterruptedException e) {
			Log.d(TAG, "Interrupted while waiting for lock RETURNING NULL");
			return -1;
		}
		if( lie == null )
		{
			mutex.unlock();
			return -1;
		}
		int q = trackingQuality;
		mutex.unlock();
		return q;
	}
	
	/**
	 * Returns camera pose as two 3 tuples. The first 3 floats being the camera position, and the 
	 * second 3 floats being a Special Orthogonal Lie representing camera orientation.
	 * @param posBuff a float buffer in which to store the position
	 * @param poffset the index into the buffer at which to store the position
	 * @param lieBuff a float buffer in which to store the lie
	 * @param loffset the index into the buffer at which to store the lie
	 * @return an int representing tracking quality, or -1 if called in an invalid state
	 */
	public int getCameraPose(float[] posBuff, int poffset, float[] lieBuff, int loffset)
	{
		try {
			mutex.lockInterruptibly();
		} catch (InterruptedException e) {
			Log.d(TAG, "Interrupted while waiting for lock RETURNING NULL");
			return -1;
		}
		if(position == null )
		{
			mutex.unlock();
			return -1;
		}
		if(lie == null )
		{
			mutex.unlock();
			return -1;
		}
		int q = trackingQuality;
		// TODO: Document this feature
		if(posBuff != null)
		{
			posBuff[poffset + 0] = position.x;
			posBuff[poffset + 1] = position.y;
			posBuff[poffset + 2] = position.z;
		}
		if(lieBuff != null)
		{
			lieBuff[loffset + 0] = lie.x;
			lieBuff[loffset + 1] = lie.y;
			lieBuff[loffset + 2] = lie.z;
		}
		mutex.unlock();
		return q;
	}
	
	private void processPose(String pose)
	{
		String[] posStr = pose.split(",");
		if(posStr.length != 7)
		{
			Log.e(TAG, "Did not find 7-tuple pose!");
			return;
		}
		float x,y,z,a,b,c;
		int q;
		try {
		x = Float.valueOf(posStr[0]);
		y = Float.valueOf(posStr[1]);
		z = Float.valueOf(posStr[2]);
		a = Float.valueOf(posStr[3]);
		b = Float.valueOf(posStr[4]);
		c = Float.valueOf(posStr[5]);
		q = Integer.valueOf(posStr[6]);
		} catch(NumberFormatException ex) {
			for(int i = 0; i < posStr.length; i++)
			{
				if(posStr[i].equalsIgnoreCase("nan"))
				{
					return;
				}
			}
			Log.e(TAG, "Malformed float: " + ex.getMessage());
			Log.e(TAG, posStr.toString());
			return;
		}
		setPose(x, y, z, a, b, c, q);
	}
	
	private long timeBefore = 0l;
	private int posAmt = 0;
	private String s;
	private String strstore = "";
	private static final int BUFF_SIZE = 1024;
	private void readCameraPose(char []buf)
	{
		long after;
		int amt;
		try {
			if(timeBefore == 0l)
			{
				timeBefore = System.currentTimeMillis();
			}
			// currently optimized for less reads. tradeoff: more processing
			amt = reader.read(buf, posAmt, BUFF_SIZE - posAmt);
			if(amt > 0)
			{
				boolean foundColon = false;
				 
				if(buf[posAmt + amt - 1] == ':') // brief empirical analysis shows if there's a colon, it's often here
				{
					foundColon = true;
				}
				else
				{
					for(int i = posAmt; i < (posAmt + amt); i++)
					{
						if(buf[i] == ':')
						{
							foundColon = true;
							break;
						}
					}
				}
				
				if(foundColon)
				{
					if(timeBefore != 0l)
					{
						after = System.currentTimeMillis();
						totalThroughputTimeMillis += (after - timeBefore);
						posesSeen++;
					}
					s = "";
					for(int i = 0; i < posAmt + amt; i++)
					{
						s += buf[i];
					}
					//Log.d(TAG, "Buff was \"" + s + "\"");
					boolean storeNext = buf[posAmt + amt - 1] != ':';
					String nextStore = "";
					int endIndex = s.length() - 1;
					for(int i = endIndex; i >= 0; i--)
					{
						if((s.charAt(i) == ':') || (i == 0))
						{
							if(i == endIndex)
							{
								endIndex--;
							}
							else if(storeNext)
							{
								storeNext = false;
								nextStore = s.substring(i + 1, endIndex + 1);
								endIndex = i - 1;
							}
							else
							{
								s = (i == 0) ? s.substring(i, endIndex + 1) : s.substring( i + 1, endIndex + 1);
								break;
							}
						}
					}
					if(strstore.length() != 0)
					{
						s = s + strstore;
					}
					strstore = nextStore;
					//Log.d(TAG, "new strstore is \"" + strstore + "\"");
					//Log.d(TAG, "new pose string is \"" + s + "\"");
					processPose(s);
					
					posAmt = 0;
					timeBefore = 0l;
				}
				else
				{
					posAmt += amt;
				}
			}
		} catch(IOException e) {
			// Downgraded from an error to a warning because I use this 
			// to stop the thread by closing the socket
			Log.w(TAG, "Failed to read pose from socket: " + e.getMessage());
		}
	}
	
	@Override
	public void run()
	{
		char []buf = new char[BUFF_SIZE];
		while(this.reader == null)
		{
			Thread.yield();
		}
		while(!isTorndown)
		{
			if(!isInitialized)
			{
				checkInit(buf);
			}
			else
			{
				readCameraPose(buf);
			}
		}
	}
}
